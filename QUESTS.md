<details>
<summary>The following contains an overview on all 47 supported quests.</summary>

* [AddAcceptsCards](#qAddAcceptsCards)
* [AddAirCompressor](#qAddAirCompressor)
* [AddAmenityCover](#qAddAmenityCover)
* [AddAtmCashIn](#qAddAtmCashIn)
* [AddAtmOperator](#qAddAtmOperator)
* [AddBenchBackrest](#qAddBenchBackrest)
* [AddBenchStatusOnBusStop](#qAddBenchStatusOnBusStop)
* [AddBikeParkingAccess](#qAddBikeParkingAccess)
* [AddBikeParkingCapacity](#qAddBikeParkingCapacity)
* [AddBikeParkingCover](#qAddBikeParkingCover)
* [AddBikeParkingFee](#qAddBikeParkingFee)
* [AddBikeParkingType](#qAddBikeParkingType)
* [AddBoardType](#qAddBoardType)
* [AddBuildingLevels](#qAddBuildingLevels)
* [AddBuildingType](#qAddBuildingType)
* [AddBusStopName](#qAddBusStopName)
* [AddBusStopShelter](#qAddBusStopShelter)
* [AddCameraType](#qAddCameraType)
* [AddCarWashType](#qAddCarWashType)
* [AddChargingStationCapacity](#qAddChargingStationCapacity)
* [AddChargingStationOperator](#qAddChargingStationOperator)
* [AddDefibrillatorLocation](#qAddDefibrillatorLocation)
* [AddFerryAccessMotorVehicle](#qAddFerryAccessMotorVehicle)
* [AddFerryAccessPedestrian](#qAddFerryAccessPedestrian)
* [AddForestLeafType](#qAddForestLeafType)
* [AddHairdresserCustomers](#qAddHairdresserCustomers)
* [AddInformationToTourism](#qAddInformationToTourism)
* [AddIsBuildingUnderground](#qAddIsBuildingUnderground)
* [AddMemorialType](#qAddMemorialType)
* [AddOrchardProduce](#qAddOrchardProduce)
* [AddParcelLockerBrand](#qAddParcelLockerBrand)
* [AddParcelLockerMailIn](#qAddParcelLockerMailIn)
* [AddParcelLockerPickup](#qAddParcelLockerPickup)
* [AddPlaygroundAccess](#qAddPlaygroundAccess)
* [AddPostboxCollectionTimes](#qAddPostboxCollectionTimes)
* [AddPowerPolesMaterial](#qAddPowerPolesMaterial)
* [AddRoadName](#qAddRoadName)
* [AddSport](#qAddSport)
* [AddStepCount](#qAddStepCount)
* [AddStepsIncline](#qAddStepsIncline)
* [AddSummitRegister](#qAddSummitRegister)
* [AddToiletAvailability](#qAddToiletAvailability)
* [AddToiletsFee](#qAddToiletsFee)
* [AddTrafficCalmingType](#qAddTrafficCalmingType)
* [AddTrafficSignalsButton](#qAddTrafficSignalsButton)
* [AddTreeLeafType](#qAddTreeLeafType)
* [AddVegetarian](#qAddVegetarian)
</details>

# Overview

| Quest | All | Pending | Solved | Original |
|-------|-----|---------|--------|----------|
| AddAcceptsCards | ✅ | ✅ | ✅ | ✅ |
| AddAirCompressor | ✅ | ✅ | ✅ | ✅ |
| AddAmenityCover | ✅ | ✅ | ✅ | ✅ |
| AddAtmCashIn | ✅ | ✅ | ✅ | ✅ |
| AddAtmOperator | ✅ | ✅ | ✅ | ✅ |
| AddBenchBackrest | ❌ | ✅ | ✅ | ✅ |
| AddBenchStatusOnBusStop | ✅ | ✅ | ✅ | ✅ |
| AddBikeParkingAccess | ✅ | ✅ | ✅ | ✅ |
| AddBikeParkingCapacity | ✅ | ✅ | ✅ | ✅ |
| AddBikeParkingCover | ❌ | ✅ | ❌ | ✅ |
| AddBikeParkingFee | ❌ | ✅ | ❌ | ✅ |
| AddBikeParkingType | ❌ | ✅ | ❌ | ✅ |
| AddBoardType | ✅ | ✅ | ✅ | ✅ |
| AddBuildingLevels | ❌ | ✅ | ❌ | ✅ |
| AddBuildingType | ❌ | ✅ | ❌ | ✅ |
| AddBusStopName | ❌ | ✅ | ✅ | ✅ |
| AddBusStopShelter | ❌ | ✅ | ❌ | ✅ |
| AddCameraType | ✅ | ✅ | ✅ | ✅ |
| AddCarWashType | ❌ | ✅ | ❌ | ✅ |
| AddChargingStationCapacity | ✅ | ✅ | ✅ | ✅ |
| AddChargingStationOperator | ❌ | ✅ | ❌ | ✅ |
| AddDefibrillatorLocation | ✅ | ✅ | ✅ | ✅ |
| AddFerryAccessMotorVehicle | ✅ | ✅ | ✅ | ✅ |
| AddFerryAccessPedestrian | ✅ | ✅ | ✅ | ✅ |
| AddForestLeafType | ❌ | ✅ | ❌ | ❌ |
| AddHairdresserCustomers | ✅ | ✅ | ✅ | ✅ |
| AddInformationToTourism | ✅ | ✅ | ✅ | ✅ |
| AddIsBuildingUnderground | ✅ | ✅ | ✅ | ✅ |
| AddMemorialType | ❌ | ✅ | ✅ | ✅ |
| AddOrchardProduce | ❌ | ✅ | ❌ | ✅ |
| AddParcelLockerBrand ❗️ | ✅ | ✅ | ✅ | ✅ |
| AddParcelLockerMailIn ❗️ | ✅ | ✅ | ✅ | ✅ |
| AddParcelLockerPickup ❗️ | ✅ | ✅ | ✅ | ✅ |
| AddPlaygroundAccess | ✅ | ✅ | ✅ | ✅ |
| AddPostboxCollectionTimes | ❌ | ✅ | ❌ | ❌ |
| AddPowerPolesMaterial | ✅ | ✅ | ✅ | ✅ |
| AddRoadName | ❌ | ✅ | ❌ | ✅ |
| AddSport | ❌ | ✅ | ❌ | ✅ |
| AddStepCount | ✅ | ✅ | ✅ | ✅ |
| AddStepsIncline | ✅ | ✅ | ✅ | ✅ |
| AddSummitRegister | ❌ | ✅ | ❌ | ❌ |
| AddToiletAvailability | ✅ | ✅ | ✅ | ✅ |
| AddToiletsFee | ✅ | ✅ | ✅ | ✅ |
| AddTrafficCalmingType | ✅ | ✅ | ✅ | ✅ |
| AddTrafficSignalsButton | ✅ | ✅ | ✅ | ✅ |
| AddTreeLeafType | ✅ | ✅ | ✅ | ✅ |
| AddVegetarian | ❌ | ✅ | ❌ | ✅ |

# <img src="src/main/resources/sc_svg/card.svg" width="24"> <a name="qAddAcceptsCards"></a>AddAcceptsCards

<details>

This quest has a priority of **136.0** and will modify [["payment:debit_cards","payment:credit_cards"]](https://wiki.openstreetmap.org/wiki/["payment:debit_cards","payment:credit_cards"]) of elements with [Key:payment](https://wiki.openstreetmap.org/wiki/Key:payment), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"][!"payment:credit_cards"][!"payment:debit_cards"]["payment:others"!="no"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["amenity"~"^restaurant$|^cafe$|^fast_food$|^ice_cream$|^food_court$|^pub$|^bar$"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:credit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"][!"seasonal"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"][!"fee"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["noname"="yes"]["access"!~"^private$|^no$"](area.municipality);
nw["shop"]["shop"!~"^no$|^vacant$|^mall$)"]["payment:debit_cards"][!"brand"][!"wikipedia:brand"][!"wikidata:brand"]["seasonal"="no"]["fee"!="no"]["name:signed"="no"]["access"!~"^private$|^no$"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/accepts_cards/AddAcceptsCards.kt).
```
nodes, ways with (
          amenity ~ restaurant|cafe|fast_food|ice_cream|food_court|pub|bar
          or (shop and shop !~ no|vacant|mall)
        )
        and !payment:credit_cards and !payment:debit_cards and payment:others != no
        and !brand and !wikipedia:brand and !wikidata:brand
        and (!seasonal or seasonal = no)
        and (name or noname = yes or name:signed = no)
        and access !~ private|no
```

</details>

# <img src="src/main/resources/sc_svg/car_air_compressor.svg" width="24"> <a name="qAddAirCompressor"></a>AddAirCompressor

<details>

This quest has a priority of **43.0** and will modify [compressed_air](https://wiki.openstreetmap.org/wiki/compressed_air) of elements with [Key:compressed_air](https://wiki.openstreetmap.org/wiki/Key:compressed_air), and it will count towards the following trophies: CAR, BICYCLIST. This quest is resurveyed every 6 years.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
nw["amenity"="fuel"](area.municipality);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(nw["amenity"="fuel"]["compressed_air"](area.municipality);)->.all;
(nw["amenity"="fuel"]["compressed_air"](area.municipality)(newer:"2019-01-29T04:56:10Z");)->.new;

(
  nw["amenity"="fuel"][!"compressed_air"](area.municipality);
  (.all; - .new;);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
nw["amenity"="fuel"]["compressed_air"](area.municipality)(newer:"2019-01-29T04:56:10Z");
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/air_pump/AddAirCompressor.kt).
```
nodes, ways with
        amenity = fuel
        and (
            !compressed_air
            or compressed_air older today -6 years
        )
```

</details>

# <img src="src/main/resources/sc_svg/picnic_table_cover.svg" width="24"> <a name="qAddAmenityCover"></a>AddAmenityCover

<details>

This quest has a priority of **12.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:covered](https://wiki.openstreetmap.org/wiki/Key:covered), and it will count towards the following trophies: OUTDOORS.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["leisure"="picnic_table"]["access"!~"private|no"][!"seasonal"]
  (area.municipality);
  node["leisure"="picnic_table"]["access"!~"private|no"]["seasonal"="no"]
  (area.municipality);
  node["amenity"="bbq"]["access"!~"private|no"][!"seasonal"]
  (area.municipality);
  node["amenity"="bbq"]["access"!~"private|no"]["seasonal"="no"]
  (area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["leisure"="picnic_table"]["access"!~"private|no"][!"covered"][!"seasonal"]
  (area.municipality);
  node["leisure"="picnic_table"]["access"!~"private|no"][!"covered"]["seasonal"="no"]
  (area.municipality);
  node["amenity"="bbq"]["access"!~"private|no"][!"covered"][!"seasonal"]
  (area.municipality);
  node["amenity"="bbq"]["access"!~"private|no"][!"covered"]["seasonal"="no"]
  (area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["leisure"="picnic_table"]["access"!~"private|no"]["covered"][!"seasonal"]
  (area.municipality);
  node["leisure"="picnic_table"]["access"!~"private|no"]["covered"]["seasonal"="no"]
  (area.municipality);
  node["amenity"="bbq"]["access"!~"private|no"]["covered"][!"seasonal"]
  (area.municipality);
  node["amenity"="bbq"]["access"!~"private|no"]["covered"]["seasonal"="no"]
  (area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/amenity_cover/AddAmenityCover.kt).
```
        nodes with
          (leisure = picnic_table
           or amenity = bbq)
          and access !~ private|no
          and !covered
          and (!seasonal or seasonal = no)
    

```

</details>

# <img src="src/main/resources/sc_svg/money.svg" width="24"> <a name="qAddAtmCashIn"></a>AddAtmCashIn

<details>

This quest has a priority of **90.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:cash_in](https://wiki.openstreetmap.org/wiki/Key:cash_in), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["amenity"="atm"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["amenity"="atm"][!"cash_in"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["amenity"="atm"]["cash_in"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/atm_cashin/AddAtmCashIn.kt).
```
nodes with amenity = atm and !cash_in
```

</details>

# <img src="src/main/resources/sc_svg/money.svg" width="24"> <a name="qAddAtmOperator"></a>AddAtmOperator

<details>

This quest has a priority of **89.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=atm](https://wiki.openstreetmap.org/wiki/Tag:amenity=atm), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["amenity"="atm"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["amenity"="atm"][!operator][!name][!brand](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["amenity"="atm"][operator](area.municipality);
  node["amenity"="atm"][name](area.municipality);
  node["amenity"="atm"][brand](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/atm_operator/AddAtmOperator.kt).
```
nodes with amenity = atm and !operator and !name and !brand
```

</details>

# <img src="src/main/resources/sc_svg/bench_poi.svg" width="24"> <a name="qAddBenchBackrest"></a>AddBenchBackrest

<details>

This quest has a priority of **11.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=bench](https://wiki.openstreetmap.org/wiki/Tag:amenity=bench), and it will count towards the following trophies: PEDESTRIAN, OUTDOORS.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["amenity"="bench"][!"area"][!"backrest"][!"bench:type"][!"seasonal"]["access"!="private"](area.municipality);
  nw["amenity"="bench"][!"area"][!"backrest"][!"bench:type"][!"seasonal"]["access"!="no"](area.municipality);
  nw["amenity"="bench"][!"area"][!"backrest"][!"bench:type"]["seasonal"="no"]["access"!="private"](area.municipality);
  nw["amenity"="bench"][!"area"][!"backrest"][!"bench:type"]["seasonal"="no"]["access"!="no"](area.municipality);
  nw["amenity"="bench"]["area"="no"][!"backrest"][!"bench:type"][!"seasonal"]["access"!="private"](area.municipality);
  nw["amenity"="bench"]["area"="no"][!"backrest"][!"bench:type"][!"seasonal"]["access"!="no"](area.municipality);
  nw["amenity"="bench"]["area"="no"][!"backrest"][!"bench:type"]["seasonal"="no"]["access"!="private"](area.municipality);
  nw["amenity"="bench"]["area"="no"][!"backrest"][!"bench:type"]["seasonal"="no"]["access"!="no"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["amenity"="bench"][!"area"]["backrest"][!"bench:type"][!"seasonal"]["access"!="private"](area.municipality);
  nw["amenity"="bench"][!"area"]["backrest"][!"bench:type"][!"seasonal"]["access"!="no"](area.municipality);
  nw["amenity"="bench"][!"area"]["backrest"][!"bench:type"]["seasonal"="no"]["access"!="private"](area.municipality);
  nw["amenity"="bench"][!"area"]["backrest"][!"bench:type"]["seasonal"="no"]["access"!="no"](area.municipality);
  nw["amenity"="bench"]["area"="no"]["backrest"][!"bench:type"][!"seasonal"]["access"!="private"](area.municipality);
  nw["amenity"="bench"]["area"="no"]["backrest"][!"bench:type"][!"seasonal"]["access"!="no"](area.municipality);
  nw["amenity"="bench"]["area"="no"]["backrest"][!"bench:type"]["seasonal"="no"]["access"!="private"](area.municipality);
  nw["amenity"="bench"]["area"="no"]["backrest"][!"bench:type"]["seasonal"="no"]["access"!="no"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/bench_backrest/AddBenchBackrest.kt).
```
nodes, ways with
          amenity = bench
          and (!area or area = no)
          and !backrest
          and !bench:type
          and (!seasonal or seasonal = no)
          and access !~ private|no
```

</details>

# <img src="src/main/resources/sc_svg/bench_public_transport.svg" width="24"> <a name="qAddBenchStatusOnBusStop"></a>AddBenchStatusOnBusStop

<details>

This quest has a priority of **3.0** and will modify [bench](https://wiki.openstreetmap.org/wiki/bench) of elements with [Key:bench](https://wiki.openstreetmap.org/wiki/Key:bench), and it will count towards the following trophies: PEDESTRIAN. This quest is resurveyed every 4 years.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"](area.municipality);
  nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"](area.municipality);
)
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality);)
-> .all_with_bench_1;
(nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality)(newer:"2021-01-28T16:56:10Z");)
-> .recently_edited_1;
  
(nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality);)
-> .all_with_bench_2;
(nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality)(newer:"2021-01-28T16:56:10Z");)
-> .recently_edited_2;

(
nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"bench"](area.municipality);
nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"bench"](area.municipality);
(.all_with_bench_1; - .recently_edited_1;);
(.all_with_bench_2; - .recently_edited_2;);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality);
nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality);
nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality)(newer:"2021-01-28T16:56:10Z");
nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"]["bench"](area.municipality)(newer:"2021-01-28T16:56:10Z");
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/bus_stop_bench/AddBenchStatusOnBusStop.kt).
```
nodes, ways, relations with
        (
          public_transport = platform
          or (highway = bus_stop and public_transport != stop_position)
        )
        and physically_present != no and naptan:BusStopType != HAR
        and (!bench or bench older today -4 years)
```

</details>

# <img src="src/main/resources/sc_svg/bicycle_parking_access.svg" width="24"> <a name="qAddBikeParkingAccess"></a>AddBikeParkingAccess

<details>

This quest has a priority of **30.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=bicycle_parking](https://wiki.openstreetmap.org/wiki/Tag:amenity=bicycle_parking), and it will count towards the following trophies: BICYCLIST.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nwr["amenity"="bicycle_parking"]["bicycle_parking"~"^building$|^lockers$|^shed$"]
  (area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nwr["amenity"="bicycle_parking"]["bicycle_parking"~"^building$|^lockers$|^shed$"][!"access"]
  (area.municipality);
  nwr["amenity"="bicycle_parking"]["bicycle_parking"~"^building$|^lockers$|^shed$"]["access"="unknown"]
  (area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nwr["amenity"="bicycle_parking"]["bicycle_parking"~"^building$|^lockers$|^shed$"]["access"]["access"!="unknown"]
  (area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/parking_access/AddBikeParkingAccess.kt).
```
nodes, ways, relations with amenity = bicycle_parking
        and bicycle_parking ~ building|lockers|shed
        and (!access or access = unknown)
```

</details>

# <img src="src/main/resources/sc_svg/bicycle_parking_capacity.svg" width="24"> <a name="qAddBikeParkingCapacity"></a>AddBikeParkingCapacity

<details>

This quest has a priority of **83.0** and will modify [capacity](https://wiki.openstreetmap.org/wiki/capacity) of elements with [Tag:amenity=bicycle_parking](https://wiki.openstreetmap.org/wiki/Tag:amenity=bicycle_parking), and it will count towards the following trophies: BICYCLIST. This quest is resurveyed every 4 years.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
nw["amenity"="bicycle_parking"]
  ["access"!~"^private$|^no$"]
  ["bicycle_parking"!~"^floor$|^informal$"]
  (area.municipality);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nw["amenity"="bicycle_parking"]
  ["access"!~"^private$|^no$"]
  ["bicycle_parking"!~"^floor$|^informal$"]
  ["bicycle_parking"~"^stands$|^wall_loops$|^safe_loops$|^handlebar_holder$"]
  ["capacity"]
  (area.municipality);
)->.all;

(
nw["amenity"="bicycle_parking"]
  ["access"!~"^private$|^no$"]
  ["bicycle_parking"!~"^floor$|^informal$"]
  ["bicycle_parking"~"^stands$|^wall_loops$|^safe_loops$|^handlebar_holder$"]
  ["capacity"]
  (area.municipality)
  (newer:"2024-11-28T18:56:10Z");
)->.newer;

(
nw["amenity"="bicycle_parking"]
  ["access"!~"^private$|^no$"]
  ["bicycle_parking"!~"^floor$|^informal$"]
  [!"capacity"]
  (area.municipality);

  (.all; - .newer;);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
nw["amenity"="bicycle_parking"]
  ["access"!~"^private$|^no$"]
  ["bicycle_parking"!~"^floor$|^informal$"]
  [!"capacity"]
  (area.municipality);

nw["amenity"="bicycle_parking"]
  ["access"!~"^private$|^no$"]
  ["bicycle_parking"!~"^floor$|^informal$"]
  ["bicycle_parking"~"^stands$|^wall_loops$|^safe_loops$|^handlebar_holder$"]
  ["capacity"]
  (area.municipality)
  (newer:"2024-11-28T18:56:10Z");
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/bike_parking_capacity/AddBikeParkingCapacity.kt).
```
nodes, ways with
         amenity = bicycle_parking
         and access !~ private|no
         and bicycle_parking !~ floor|informal
         and (
           !capacity
           or (
             bicycle_parking ~ stands|wall_loops|safe_loops|handlebar_holder
             and capacity older today -4 years
           )
         )
```

</details>

# <img src="src/main/resources/sc_svg/bicycle_parking_cover.svg" width="24"> <a name="qAddBikeParkingCover"></a>AddBikeParkingCover

<details>

This quest has a priority of **73.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=bicycle_parking](https://wiki.openstreetmap.org/wiki/Tag:amenity=bicycle_parking), and it will count towards the following trophies: BICYCLIST.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nw["amenity"="bicycle_parking"]["access"!~"private|no"][!"covered"]["bicycle_parking"!~"shed|lockers|building"](area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/bike_parking_cover/AddBikeParkingCover.kt).
```
nodes, ways with
         amenity = bicycle_parking
         and access !~ private|no
         and !covered
         and bicycle_parking !~ shed|lockers|building
```

</details>

# <img src="src/main/resources/sc_svg/bicycle_parking_fee.svg" width="24"> <a name="qAddBikeParkingFee"></a>AddBikeParkingFee

<details>

This quest has a priority of **77.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=bicycle_parking](https://wiki.openstreetmap.org/wiki/Tag:amenity=bicycle_parking), and it will count towards the following trophies: BICYCLIST. This quest is resurveyed every 8 years.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]["name"]["fee"]
  (area.municipality);) -> .all_with_fee_1;
(nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]["bicycle_parking"~"^building$|^lockers$|^shed$"]
  (area.municipality);) -> .all_with_fee_2;
(nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]
  (area.municipality)(if:number(t["capacity"])>=100);) -> .all_with_fee_3;

(nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]["name"]["fee"]
  (area.municipality)(newer:"2017-01-28T16:56:10Z");) -> .recently_edited_1;
(nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]["bicycle_parking"~"^building$|^lockers$|^shed$"]
  (area.municipality)(newer:"2017-01-28T16:56:10Z");) -> .recently_edited_2;
(nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]
  (area.municipality)(if:number(t["capacity"])>=100)(newer:"2017-01-28T16:56:10Z");) -> .recently_edited_3;

(
  nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]["name"][!"fee"][!"fee:conditional"]
  (area.municipality);
  nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"]["bicycle_parking"~"^building$|^lockers$|^shed$"][!"fee"][!"fee:conditional"]
  (area.municipality);
  nwr["amenity"="bicycle_parking"]["access"~"^yes$|^customers$|^public$"][!"fee"][!"fee:conditional"]
  (area.municipality)(if:number(t["capacity"])>=100);
  (.all_with_fee_1; - .recently_edited_1;);
  (.all_with_fee_2; - .recently_edited_2;);
  (.all_with_fee_3; - .recently_edited_3;);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/parking_fee/AddBikeParkingFee.kt).
```
nodes, ways, relations with amenity = bicycle_parking
        and access ~ yes|customers|public
        and (
            name
            or bicycle_parking ~ building|lockers|shed
            or capacity >= 100
        )
        and (
            !fee and !fee:conditional
            or fee older today -8 years
        )
```

</details>

# <img src="src/main/resources/sc_svg/bicycle_parking.svg" width="24"> <a name="qAddBikeParkingType"></a>AddBikeParkingType

<details>

This quest has a priority of **75.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:bicycle_parking](https://wiki.openstreetmap.org/wiki/Key:bicycle_parking), and it will count towards the following trophies: BICYCLIST.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nw["amenity"="bicycle_parking"]["access"!~"private|no"][!"bicycle_parking"](area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/bike_parking_type/AddBikeParkingType.kt).
```
nodes, ways with
          amenity = bicycle_parking
          and access !~ private|no
          and !bicycle_parking
```

</details>

# <img src="src/main/resources/sc_svg/board_type.svg" width="24"> <a name="qAddBoardType"></a>AddBoardType

<details>

This quest has a priority of **54.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:board_type](https://wiki.openstreetmap.org/wiki/Key:board_type), and it will count towards the following trophies: RARE, CITIZEN, OUTDOORS.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["tourism"="information"]["information"="board"]["access"!~"private|no"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["tourism"="information"]["information"="board"]["access"!~"private|no"][!"board_type"](area.municipality);
  node["tourism"="information"]["information"="board"]["access"!~"private|no"]["board_type"~"yes|board"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["tourism"="information"]["information"="board"]["access"!~"private|no"]["board_type"]["board_type"!~"yes|board"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/board_type/AddBoardType.kt).
```
nodes with
         tourism = information
         and information = board
         and access !~ private|no
         and (!board_type or board_type ~ yes|board)
```

</details>

# <img src="src/main/resources/sc_svg/building_levels.svg" width="24"> <a name="qAddBuildingLevels"></a>AddBuildingLevels

<details>

This quest has a priority of **156.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:building:levels](https://wiki.openstreetmap.org/wiki/Key:building:levels), and it will count towards the following trophies: BUILDING.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  wr["building"~"^house$|^residential$|^apartments$|^detached$|^terrace$|^dormitory$|^semi$|^semidetached_house$|^bungalow$|^school$|^civic$|^college$|^university$|^public$|^hospital$|^kindergarten$|^transportation$|^train_station$|^hotel$|^retail$|^commercial$|^office$|^manufacture$|^parking$|^farm$|^cabin$"]
  [!"building:levels"]
  [!"height"][!"roof:height"]
  [!"building:min_level"]
  [!"man_made"]
  ["location"!="underground"]
  ["ruins"!="yes"]
  (area.municipality);
  wr["building"~"^house$|^residential$|^apartments$|^detached$|^terrace$|^dormitory$|^semi$|^semidetached_house$|^bungalow$|^school$|^civic$|^college$|^university$|^public$|^hospital$|^kindergarten$|^transportation$|^train_station$|^hotel$|^retail$|^commercial$|^office$|^manufacture$|^parking$|^farm$|^cabin$"]
  [!"roof:levels"][!"roof:height"]["roof:shape"]["roof:shape"!="flat"]
  [!"height"][!"roof:height"]
  [!"building:min_level"]
  [!"man_made"]
  ["location"!="underground"]
  ["ruins"!="yes"]
  (area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/building_levels/AddBuildingLevels.kt).
```
ways, relations with
           building ~ ${BUILDINGS_WITH_LEVELS.joinToString("|")}
           and (
             !building:levels
             or !roof:levels and !roof:height and roof:shape and roof:shape != flat
           )
           and !(height and roof:height)
           and !building:min_level
           and !man_made
           and location != underground
           and ruins != yes
```

</details>

# <img src="src/main/resources/sc_svg/building.svg" width="24"> <a name="qAddBuildingType"></a>AddBuildingType

<details>

This quest has a priority of **155.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:building](https://wiki.openstreetmap.org/wiki/Key:building), and it will count towards the following trophies: BUILDING.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  wr["building"~"^yes$|^semi$|^semidetached$|^semi_detached$|^duplex$|^detached_house$|^terraced_house$|^terraced$|^townhouse$|mobile_home|^flats$|^shop$|storage_tank|tank|silo|glasshouse|collapsed|^damaged$|ruin|abandoned|disused|unclassified|undefined|unknown|^other$|fixme"]
  [!"man_made"][!"historic"][!"military"][!"power"][!"tourism"][!"attraction"][!"amenity"][!"leisure"][!"aeroway"][!"railway"][!"craft"][!"healthcare"][!"office"][!"shop"][!"description"][!"emergency"]
  ["location"!="underground"]["disused"!="yes"]["abandoned"!="yes"]["ruins"!="yes"](area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/building_type/AddBuildingType.kt).
```
ways, relations with
        building ~ yes|${BuildingType.deprecatedValues.joinToString("|")}
        and ${BuildingType.otherKeysPotentiallyDescribingBuildingType.joinToString(" and ") { "!$it" }}
        and location != underground
        and disused != yes
        and abandoned != yes
        and ruins != yes
```

</details>

# <img src="src/main/resources/sc_svg/bus_stop_name.svg" width="24"> <a name="qAddBusStopName"></a>AddBusStopName

<details>

This quest has a priority of **6.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:public_transport=platform](https://wiki.openstreetmap.org/wiki/Tag:public_transport=platform), and it will count towards the following trophies: PEDESTRIAN.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nwr["public_transport"="platform"]["bus"="yes"]
  [!"name"]["noname"!="yes"]["name:signed"!="no"](area.municipality);
nwr["highway"="bus_stop"]["public_transport"!="stop_position"]
  [!"name"]["noname"!="yes"]["name:signed"!="no"](area.municipality);
nwr["railway"~"^halt$|^station$|^tram_stop$"]
  [!"name"]["noname"!="yes"]["name:signed"!="no"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nwr["public_transport"="platform"]["bus"="yes"]
  ["name"]["noname"!="yes"]["name:signed"!="no"](area.municipality);
nwr["highway"="bus_stop"]["public_transport"!="stop_position"]
  ["name"]["noname"!="yes"]["name:signed"!="no"](area.municipality);
nwr["railway"~"^halt$|^station$|^tram_stop$"]
  ["name"]["noname"!="yes"]["name:signed"!="no"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/bus_stop_name/AddBusStopName.kt).
```
nodes, ways, relations with
        (
          public_transport = platform and bus = yes
          or highway = bus_stop and public_transport != stop_position
          or railway ~ halt|station|tram_stop
        )
        and !name and noname != yes and name:signed != no
```

</details>

# <img src="src/main/resources/sc_svg/bus_stop_shelter.svg" width="24"> <a name="qAddBusStopShelter"></a>AddBusStopShelter

<details>

This quest has a priority of **2.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:shelter](https://wiki.openstreetmap.org/wiki/Key:shelter), and it will count towards the following trophies: PEDESTRIAN. This quest is resurveyed every 4 years.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"][!"level"]["shelter"](area.municipality);)->.all1;
(nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"][!"level"]["shelter"](area.municipality);)->.all2;
(nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"](if:number(t["level"])>=0)["shelter"](area.municipality);)->.all3;
(nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"](if:number(t["level"])>=0)["shelter"](area.municipality);)->.all4;

(nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"][!"level"]["shelter"](area.municipality)(newer:"2021-01-28T16:56:10Z");)->.recently1;
(nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"][!"level"]["shelter"](area.municipality)(newer:"2021-01-28T16:56:10Z");)->.recently2;
(nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"](if:number(t["level"])>=0)["shelter"](area.municipality)(newer:"2021-01-28T16:56:10Z");)->.recently3;
(nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"](if:number(t["level"])>=0)["shelter"](area.municipality)(newer:"2021-01-28T16:56:10Z");)->.recently4;

(
  nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"][!"level"][!"shelter"](area.municipality);
  nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"][!"level"][!"shelter"](area.municipality);
  nwr["public_transport"="platform"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"](if:number(t["level"])>=0)[!"shelter"](area.municipality);
  nwr["highway"="bus_stop"]["public_transport"!="stop_position"]["physically_present"!="no"]["naptan:BusStopType"!="HAR"][!"covered"]["location"!~"underground|indoor"]["indoor"!="yes"]["tunnel"!="yes"](if:number(t["level"])>=0)[!"shelter"](area.municipality);

  (.all1; - .recently1;);
  (.all2; - .recently2;);
  (.all3; - .recently3;);
  (.all4; - .recently4;);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/bus_stop_shelter/AddBusStopShelter.kt).
```
nodes, ways, relations with
        (
          public_transport = platform
          or (highway = bus_stop and public_transport != stop_position)
        )
        and physically_present != no and naptan:BusStopType != HAR
        and !covered
        and location !~ underground|indoor
        and indoor != yes
        and tunnel != yes
        and (!level or level >= 0)
        and (!shelter or shelter older today -4 years)
```

</details>

# <img src="src/main/resources/sc_svg/surveillance_camera.svg" width="24"> <a name="qAddCameraType"></a>AddCameraType

<details>

This quest has a priority of **68.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:surveillance:type](https://wiki.openstreetmap.org/wiki/Tag:surveillance:type), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
node["surveillance:type"="camera"]["surveillance"~"public|outdoor|traffic"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["surveillance:type"="camera"]["surveillance"~"public|outdoor|traffic"][!"camera:type"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["surveillance:type"="camera"]["surveillance"~"public|outdoor|traffic"]["camera:type"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/camera_type/AddCameraType.kt).
```
nodes with
         surveillance:type = camera
         and surveillance ~ public|outdoor|traffic
         and !camera:type
```

</details>

# <img src="src/main/resources/sc_svg/car_wash.svg" width="24"> <a name="qAddCarWashType"></a>AddCarWashType

<details>

This quest has a priority of **10.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=car_wash](https://wiki.openstreetmap.org/wiki/Tag:amenity=car_wash), and it will count towards the following trophies: CAR.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["amenity"="car_wash"][!"automated"][!"self_service"](area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/car_wash_type/AddCarWashType.kt).
```
nodes, ways with amenity = car_wash and !automated and !self_service
```

</details>

# <img src="src/main/resources/sc_svg/car_charger_capacity.svg" width="24"> <a name="qAddChargingStationCapacity"></a>AddChargingStationCapacity

<details>

This quest has a priority of **92.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=charging_station](https://wiki.openstreetmap.org/wiki/Tag:amenity=charging_station), and it will count towards the following trophies: CAR.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["amenity"="charging_station"]["bicycle"!="yes"]["scooter"!="yes"]["motorcar"!="no"]["access"!~"^private$|^no$"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["amenity"="charging_station"][!"capacity"]["bicycle"!="yes"]["scooter"!="yes"]["motorcar"!="no"]["access"!~"^private$|^no$"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nw["amenity"="charging_station"]["capacity"]["bicycle"!="yes"]["scooter"!="yes"]["motorcar"!="no"]["access"!~"^private$|^no$"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/charging_station_capacity/AddChargingStationCapacity.kt).
```
nodes, ways with
          amenity = charging_station
          and !capacity
          and bicycle != yes and scooter != yes and motorcar != no
          and access !~ private|no
```

</details>

# <img src="src/main/resources/sc_svg/car_charger.svg" width="24"> <a name="qAddChargingStationOperator"></a>AddChargingStationOperator

<details>

This quest has a priority of **93.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=charging_station](https://wiki.openstreetmap.org/wiki/Tag:amenity=charging_station), and it will count towards the following trophies: CAR.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["amenity"="charging_station"][!"operator"][!"name"][!"brand"]["operator:signed"!="no"]["brand:signed"!="no"]["access"!~"^private$|^no$"](area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/charging_station_operator/AddChargingStationOperator.kt).
```
nodes, ways with
          amenity = charging_station
          and !operator and !name and !brand
          and operator:signed != no
          and brand:signed != no
          and access !~ private|no
```

</details>

# <img src="src/main/resources/sc_svg/defibrillator.svg" width="24"> <a name="qAddDefibrillatorLocation"></a>AddDefibrillatorLocation

<details>

This quest has a priority of **118.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:emergency=defibrillator](https://wiki.openstreetmap.org/wiki/Tag:emergency=defibrillator), and it will count towards the following trophies: LIFESAVER.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["emergency"="defibrillator"]["access"!~"private|no"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["emergency"="defibrillator"][!"location"][!"defibrillator:location"]["access"!~"private|no"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["emergency"="defibrillator"]["location"]["access"!~"private|no"](area.municipality);
  node["emergency"="defibrillator"]["defibrillator:location"]["access"!~"private|no"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/defibrillator/AddDefibrillatorLocation.kt).
```
nodes with
        emergency = defibrillator
        and !location and !defibrillator:location
        and access !~ private|no"
```

</details>

# <img src="src/main/resources/sc_svg/ferry.svg" width="24"> <a name="qAddFerryAccessMotorVehicle"></a>AddFerryAccessMotorVehicle

<details>

This quest has a priority of **107.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:route=ferry](https://wiki.openstreetmap.org/wiki/Tag:route=ferry), and it will count towards the following trophies: RARE, CAR.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  wr["route"="ferry"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  wr["route"="ferry"][!"motor_vehicle"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  wr["route"="ferry"]["motor_vehicle"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/ferry/AddFerryAccessMotorVehicle.kt).
```
ways, relations with route = ferry and !motor_vehicle
```

</details>

# <img src="src/main/resources/sc_svg/ferry_pedestrian.svg" width="24"> <a name="qAddFerryAccessPedestrian"></a>AddFerryAccessPedestrian

<details>

This quest has a priority of **106.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:route=ferry](https://wiki.openstreetmap.org/wiki/Tag:route=ferry), and it will count towards the following trophies: RARE, PEDESTRIAN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  wr["route"="ferry"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  wr["route"="ferry"][!"foot"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  wr["route"="ferry"]["foot"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/ferry/AddFerryAccessPedestrian.kt).
```
ways, relations with route = ferry and !foot
```

</details>

# <img src="src/main/resources/sc_svg/leaf.svg" width="24"> <a name="qAddForestLeafType"></a>AddForestLeafType

<details>

This quest has a priority of **112.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:leaf_type](https://wiki.openstreetmap.org/wiki/Key:leaf_type), and it will count towards the following trophies: OUTDOORS.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  way["natural"="tree_row"][!"leaf_type"](area.municipality);
  wr["landuse"="forest"][!"leaf_type"](area.municipality)(if:length() <= 800);
  wr["natural"="wood"][!"leaf_type"](area.municipality)(if:length() <= 800);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

The original element filter from the [StreetComplete source code](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/leaf_detail/AddForestLeafType.kt) is currently missing.

</details>

# <img src="src/main/resources/sc_svg/hairdresser.svg" width="24"> <a name="qAddHairdresserCustomers"></a>AddHairdresserCustomers

<details>

This quest has a priority of **82.0** and will modify [ ](https://wiki.openstreetmap.org/wiki/ ) of elements with [Tag:shop=hairdresser](https://wiki.openstreetmap.org/wiki/Tag:shop=hairdresser), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["shop"="hairdresser"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nw["shop"="hairdresser"]["hairdresser"!="barber"][!"female"][!"male"][!"male:signed"][!"female:signed"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nw["shop"="hairdresser"]["hairdresser"!="barber"]["female"](area.municipality);
  nw["shop"="hairdresser"]["hairdresser"!="barber"]["male"](area.municipality);
  nw["shop"="hairdresser"]["hairdresser"!="barber"]["male:signed"](area.municipality);
  nw["shop"="hairdresser"]["hairdresser"!="barber"]["female:signed"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/hairdresser/AddHairdresserCustomers.kt).
```
nodes, ways with
          (
              shop = hairdresser
              and hairdresser != barber
              and !female and !male
              and !male:signed and !female:signed
          )
```

</details>

# <img src="src/main/resources/sc_svg/information.svg" width="24"> <a name="qAddInformationToTourism"></a>AddInformationToTourism

<details>

This quest has a priority of **48.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:tourism=information](https://wiki.openstreetmap.org/wiki/Tag:tourism=information), and it will count towards the following trophies: RARE, CITIZEN, OUTDOORS.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
 nw["tourism"="information"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
 nw["tourism"="information"][!"information"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
 nw["tourism"="information"]["information"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/tourism_information/AddInformationToTourism.kt).
```
nodes, ways with tourism = information and !information
```

</details>

# <img src="src/main/resources/sc_svg/building_underground.svg" width="24"> <a name="qAddIsBuildingUnderground"></a>AddIsBuildingUnderground

<details>

This quest has a priority of **31.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:location](https://wiki.openstreetmap.org/wiki/Key:location), and it will count towards the following trophies: BUILDING.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  wr["building"]["layer"~"-[0-9]+"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  wr["building"]["layer"~"-[0-9]+"][!"location"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  wr["building"]["layer"~"-[0-9]+"]["location"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/building_underground/AddIsBuildingUnderground.kt).
```
ways, relations with building and layer ~ -[0-9]+ and !location
```

</details>

# <img src="src/main/resources/sc_svg/memorial.svg" width="24"> <a name="qAddMemorialType"></a>AddMemorialType

<details>

This quest has a priority of **27.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:memorial](https://wiki.openstreetmap.org/wiki/Key:memorial), and it will count towards the following trophies: CITIZEN.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nwr["historic"="memorial"][!"memorial"][!"memorial:type"](area.municipality);
  nwr["historic"="memorial"]["memorial"="yes"][!"memorial:type"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  nwr["historic"="memorial"]["memorial"!="yes"][!"memorial:type"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/memorial_type/AddMemorialType.kt).
```
nodes, ways, relations with
          historic=memorial
          and (!memorial or memorial=yes)
          and !memorial:type
```

</details>

# <img src="src/main/resources/sc_svg/apple.svg" width="24"> <a name="qAddOrchardProduce"></a>AddOrchardProduce

<details>

This quest has a priority of **2.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:landuse=orchard](https://wiki.openstreetmap.org/wiki/Tag:landuse=orchard), and it will count towards the following trophies: OUTDOORS.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  wr["landuse"="orchard"][!"trees"][!"produce"][!"crop"]["orchard"!="meadow_orchard"]
  (area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/orchard_produce/AddOrchardProduce.kt).
```
ways, relations with landuse = orchard
        and !trees and !produce and !crop
        and orchard != meadow_orchard
```

</details>

# <img src="src/main/resources/sc_svg/parcel_locker_brand.svg" width="24"> <a name="qAddParcelLockerBrand"></a>AddParcelLockerBrand
ℹ️ This quest is deprecated and no longer available in the current version of StreetComplete.

<details>

This quest has a priority of **79.2** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=parcel_locker](https://wiki.openstreetmap.org/wiki/Tag:amenity=parcel_locker), and it will count towards the following trophies: POSTMAN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(node["amenity"="parcel_locker"](area.municipality););
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(node["amenity"="parcel_locker"][!"brand"][!"name"][!"operator"](area.municipality););
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(node["amenity"="parcel_locker"]["brand"](area.municipality);)->.b;
(node["amenity"="parcel_locker"]["name"](area.municipality);)->.n;
(node["amenity"="parcel_locker"]["operator"](area.municipality);)->.o;

(.o;.b;.n;);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/parcel_locker_brand/AddParcelLockerBrand.kt).
```
nodes with amenity = parcel_locker and !brand and !name and !operator
```

</details>

# <img src="src/main/resources/sc_svg/parcel_locker_deposit.svg" width="24"> <a name="qAddParcelLockerMailIn"></a>AddParcelLockerMailIn
ℹ️ This quest is deprecated and no longer available in the current version of StreetComplete.

<details>

This quest has a priority of **79.6** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=parcel_locker](https://wiki.openstreetmap.org/wiki/Tag:amenity=parcel_locker), and it will count towards the following trophies: POSTMAN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(node["amenity"="parcel_locker"](area.municipality););
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(node["amenity"="parcel_locker"][!"parcel_mail_in"](area.municipality););
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(node["amenity"="parcel_locker"]["parcel_mail_in"](area.municipality););
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/parcel_locker_mail_in/AddParcelLockerMailIn.kt).
```
nodes with amenity = parcel_locker and !parcel_mail_in
```

</details>

# <img src="src/main/resources/sc_svg/parcel_locker_pickup.svg" width="24"> <a name="qAddParcelLockerPickup"></a>AddParcelLockerPickup
ℹ️ This quest is deprecated and no longer available in the current version of StreetComplete.

<details>

This quest has a priority of **79.4** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:amenity=parcel_locker](https://wiki.openstreetmap.org/wiki/Tag:amenity=parcel_locker), and it will count towards the following trophies: POSTMAN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(node["amenity"="parcel_locker"](area.municipality););
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(node["amenity"="parcel_locker"][!"parcel_pickup"](area.municipality););
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(node["amenity"="parcel_locker"]["parcel_pickup"](area.municipality););
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/parcel_locker_pickup/AddParcelLockerPickup.kt).
```
nodes with amenity = parcel_locker and !parcel_pickup
```

</details>

# <img src="src/main/resources/sc_svg/playground.svg" width="24"> <a name="qAddPlaygroundAccess"></a>AddPlaygroundAccess

<details>

This quest has a priority of **50.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:leisure=playground](https://wiki.openstreetmap.org/wiki/Tag:leisure=playground), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(nwr["leisure"="playground"](area.municipality););
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nwr["leisure"="playground"][!"access"](area.municipality);
  nwr["leisure"="playground"]["access"="unknown"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(nwr["leisure"="playground"]["access"!="unknown"](area.municipality););
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/playground_access/AddPlaygroundAccess.kt).
```
nodes, ways, relations with leisure = playground and (!access or access = unknown)
```

</details>

# <img src="src/main/resources/sc_svg/mail.svg" width="24"> <a name="qAddPostboxCollectionTimes"></a>AddPostboxCollectionTimes

<details>

This quest has a priority of **51.0** and will modify [collection_times](https://wiki.openstreetmap.org/wiki/collection_times) of elements with [Key:collection_times](https://wiki.openstreetmap.org/wiki/Key:collection_times), and it will count towards the following trophies: POSTMAN. This quest is resurveyed every 2 years.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["amenity"="post_box"]["access"!~"private|no"]["collection_times:signed"!="no"][!"collection_times"](area.municipality);

  node["amenity"="post_box"]["access"!~"private|no"]["collection_times:signed"!="no"]["collection_times"](area.municipality)->.all;
  node["amenity"="post_box"]["access"!~"private|no"]["collection_times:signed"!="no"]["collection_times"](area.municipality)(newer:"2023-01-29T03:56:10Z")->.newerCollectionTimes;
  
  (.all; - .newerCollectionTimes;);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

The original element filter from the [StreetComplete source code](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/postbox_collection_times/AddPostboxCollectionTimes.kt) is currently missing.

</details>

# <img src="src/main/resources/sc_svg/power.svg" width="24"> <a name="qAddPowerPolesMaterial"></a>AddPowerPolesMaterial

<details>

This quest has a priority of **30.0** and will modify [Key:material](https://wiki.openstreetmap.org/wiki/Key:material) of elements with [Tag:power=pole](https://wiki.openstreetmap.org/wiki/Tag:power=pole), and it will count towards the following trophies: BUILDING.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["power"="pole"](area.municipality);
  node["man_made"="utility_pole"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["power"="pole"][!"material"](area.municipality);
  node["man_made"="utility_pole"][!"material"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["power"="pole"]["material"](area.municipality);
  node["man_made"="utility_pole"]["material"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/powerpoles_material/AddPowerPolesMaterial.kt).
```
nodes with
          (power = pole or man_made = utility_pole)
          and !material
```

</details>

# <img src="src/main/resources/sc_svg/street_name.svg" width="24"> <a name="qAddRoadName"></a>AddRoadName

<details>

This quest has a priority of **101.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:name](https://wiki.openstreetmap.org/wiki/Key:name), and it will count towards the following trophies: CAR, PEDESTRIAN, POSTMAN.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  way["highway"~"^primary$|^secondary$|^tertiary$|^unclassified$|^residential$|^living_street$|^pedestrian$|^busway$"]
  [!"name"][!"name:left"][!"name:right"]
  [!"ref"]
  ["noname"!="yes"]
  ["name:signed"!="no"]
  [!"junction"]
  ["area"!="yes"]
  ["access"!~"^private$|^no$"]
  (area.municipality);
  way["highway"~"^primary$|^secondary$|^tertiary$|^unclassified$|^residential$|^living_street$|^pedestrian$|^busway$"]
  [!"name"][!"name:left"][!"name:right"]
  [!"ref"]
  ["noname"!="yes"]
  ["name:signed"!="no"]
  [!"junction"]
  ["area"!="yes"]
  ["foot"]["foot"!~"^private$|^no$"]
  (area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/road_name/AddRoadName.kt).
```
ways with
          highway ~ primary|secondary|tertiary|unclassified|residential|living_street|pedestrian|busway
          and !name and !name:left and !name:right
          and !ref
          and noname != yes
          and name:signed != no
          and !junction
          and area != yes
          and (
            access !~ private|no
            or foot and foot !~ private|no
          )
```

</details>

# <img src="src/main/resources/sc_svg/sport.svg" width="24"> <a name="qAddSport"></a>AddSport

<details>

This quest has a priority of **15.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:sport](https://wiki.openstreetmap.org/wiki/Key:sport), and it will count towards the following trophies: OUTDOORS.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  way["leisure"="pitch"][!"sport"]["access"!~"private|no"](area.municipality);
  way["leisure"="pitch"]["access"!~"private|no"]["sport"="football"](area.municipality);
  way["leisure"="pitch"]["access"!~"private|no"]["sport"="hockey"](area.municipality);
  way["leisure"="pitch"]["access"!~"private|no"]["sport"="skating"](area.municipality);
  way["leisure"="pitch"]["access"!~"private|no"]["sport"="team_handball"](area.municipality);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/sport/AddSport.kt).
```
ways with
          leisure = pitch
          and (!sport or sport ~ football|skating|hockey|team_handball)
          and access !~ private|no
```

</details>

# <img src="src/main/resources/sc_svg/steps_count.svg" width="24"> <a name="qAddStepCount"></a>AddStepCount

<details>

This quest has a priority of **158.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:step_count](https://wiki.openstreetmap.org/wiki/Key:step_count), and it will count towards the following trophies: PEDESTRIAN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  way["highway"="steps"]["access"!~"private|no"][!"indoor"][!"conveying"](area.municipality);
  way["highway"="steps"]["access"!~"private|no"]["indoor"!="no"][!"conveying"](area.municipality);	
  way["highway"="steps"]["access"!~"private|no"][!"indoor"]["conveying"!="no"](area.municipality);
  way["highway"="steps"]["access"!~"private|no"]["indoor"!="no"]["conveying"!="no"](area.municipality);	
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  way["highway"="steps"]["access"!~"private|no"][!"indoor"][!"conveying"][!"step_count"](area.municipality);
  way["highway"="steps"]["access"!~"private|no"]["indoor"!="no"][!"conveying"][!"step_count"](area.municipality);	
  way["highway"="steps"]["access"!~"private|no"][!"indoor"]["conveying"!="no"][!"step_count"](area.municipality);
  way["highway"="steps"]["access"!~"private|no"]["indoor"!="no"]["conveying"!="no"][!"step_count"](area.municipality);	
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  way["highway"="steps"]["access"!~"private|no"][!"indoor"][!"conveying"]["step_count"](area.municipality);
  way["highway"="steps"]["access"!~"private|no"]["indoor"!="no"][!"conveying"]["step_count"](area.municipality);	
  way["highway"="steps"]["access"!~"private|no"][!"indoor"]["conveying"!="no"]["step_count"](area.municipality);
  way["highway"="steps"]["access"!~"private|no"]["indoor"!="no"]["conveying"!="no"]["step_count"](area.municipality);	
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/step_count/AddStepCount.kt).
```
ways with highway = steps
         and (!indoor or indoor = no)
         and access !~ private|no
         and (!conveying or conveying = no)
         and !step_count
```

</details>

# <img src="src/main/resources/sc_svg/steps.svg" width="24"> <a name="qAddStepsIncline"></a>AddStepsIncline

<details>

This quest has a priority of **24.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:incline](https://wiki.openstreetmap.org/wiki/Key:incline), and it will count towards the following trophies: PEDESTRIAN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
way["highway"="steps"][!"indoor"]["area"!="yes"]["access"!~"private|no"](area.municipality);
way["highway"="steps"]["indoor"="no"]["area"!="yes"]["access"!~"private|no"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
 way["highway"="steps"][!"indoor"]["area"!="yes"]["access"!~"private|no"][!"incline"](area.municipality);
 way["highway"="steps"]["indoor"="no"]["area"!="yes"]["access"!~"private|no"][!"incline"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
 way["highway"="steps"][!"indoor"]["area"!="yes"]["access"!~"private|no"]["incline"](area.municipality);
 way["highway"="steps"]["indoor"="no"]["area"!="yes"]["access"!~"private|no"]["incline"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/incline_direction/AddStepsIncline.kt).
```
ways with highway = steps
         and (!indoor or indoor = no)
         and area != yes
         and access !~ private|no
         and !incline
```

</details>

# <img src="src/main/resources/sc_svg/peak.svg" width="24"> <a name="qAddSummitRegister"></a>AddSummitRegister

<details>

This quest has a priority of **111.0** and will modify [summit:register](https://wiki.openstreetmap.org/wiki/summit:register) of elements with [Key:summit:register](https://wiki.openstreetmap.org/wiki/Key:summit:register), and it will count towards the following trophies: RARE, OUTDOORS. This quest is resurveyed every 4 years.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
way["highway"="path"]["sac_scale"~"mountain_hiking|demanding_mountain_hiking|alpine_hiking|demanding_alpine_hiking|difficult_alpine_hiking"](area.municipality)->.hikingPaths;
rel["route"="hiking"](area.municipality)->.hikingRoutes;

(
  node(around.hikingPaths:10)["natural"="peak"]["name"][!"summit:register"](area.municipality);
  node(around.hikingRoutes:10)["natural"="peak"]["name"][!"summit:register"](area.municipality);
  
  node(around.hikingPaths:10)["natural"="peak"]["name"]["summit:register"](area.municipality)(newer:"2021-01-28T15:56:10Z");
  node(around.hikingRoutes:10)["natural"="peak"]["name"]["summit:register"](area.municipality)(newer:2021-01-28T15:56:10Z);
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

The original element filter from the [StreetComplete source code](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/summit/AddSummitRegister.kt) is currently missing.

</details>

# <img src="src/main/resources/sc_svg/toilets.svg" width="24"> <a name="qAddToiletAvailability"></a>AddToiletAvailability

<details>

This quest has a priority of **123.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:toilets](https://wiki.openstreetmap.org/wiki/Key:toilets), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nw["shop"~"mall|department_store"](area.municipality);
nw["highway"~"services|rest_area"](area.municipality);
nw["tourism"~"camp_site|caravan_site"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nw["shop"~"mall|department_store"][!"toilets"](area.municipality);
  nw["highway"~"services|rest_area"][!"toilets"](area.municipality);
  nw["tourism"~"camp_site|caravan_site"][!"toilets"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nw["shop"~"mall|department_store"]["toilets"](area.municipality);
  nw["highway"~"services|rest_area"]["toilets"](area.municipality);
  nw["tourism"~"camp_site|caravan_site"]["toilets"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/toilet_availability/AddToiletAvailability.kt).
```
nodes, ways with
        (
          shop ~ mall|department_store
          or highway ~ services|rest_area
          or tourism ~ camp_site|caravan_site
        )
        and !toilets
```

</details>

# <img src="src/main/resources/sc_svg/toilet_fee.svg" width="24"> <a name="qAddToiletsFee"></a>AddToiletsFee

<details>

This quest has a priority of **124.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:fee](https://wiki.openstreetmap.org/wiki/Key:fee), and it will count towards the following trophies: CITIZEN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
nw["amenity"="toilets"]["access"!~"^private$|^customers$"][!"seasonal"](area.municipality);
nw["amenity"="toilets"]["access"!~"^private$|^customers$"]["seasonal"="no"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
nw["amenity"="toilets"]["access"!~"^private$|^customers$"][!"fee"][!"seasonal"](area.municipality);
nw["amenity"="toilets"]["access"!~"^private$|^customers$"][!"fee"]["seasonal"="no"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
 nw["amenity"="toilets"]["access"!~"private|customers"][!"seasonal"]["fee"](area.municipality);
 nw["amenity"="toilets"]["access"!~"private|customers"]["seasonal"="no"]["fee"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/toilets_fee/AddToiletsFee.kt).
```
nodes, ways with
          amenity = toilets
          and access !~ private|customers
          and !fee
          and (!seasonal or seasonal = no)
```

</details>

# <img src="src/main/resources/sc_svg/car_bumpy.svg" width="24"> <a name="qAddTrafficCalmingType"></a>AddTrafficCalmingType

<details>

This quest has a priority of **21.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:traffic_calming](https://wiki.openstreetmap.org/wiki/Key:traffic_calming), and it will count towards the following trophies: PEDESTRIAN, CAR.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["traffic_calming"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
  node["traffic_calming"="yes"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  node["traffic_calming"]["traffic_calming"!="yes"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/traffic_calming_type/AddTrafficCalmingType.kt).
```
nodes with traffic_calming = yes
```

</details>

# <img src="src/main/resources/sc_svg/traffic_lights_button.svg" width="24"> <a name="qAddTrafficSignalsButton"></a>AddTrafficSignalsButton

<details>

This quest has a priority of **46.0** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Tag:highway=traffic_signals](https://wiki.openstreetmap.org/wiki/Tag:highway=traffic_signals), and it will count towards the following trophies: PEDESTRIAN.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
node["crossing"="traffic_signals"]["highway"~"^crossing$|^traffic_signals$"]["foot"!="no"](area.municipality);
node["crossing:signals"="yes"]["highway"~"^crossing$|^traffic_signals$"]["foot"!="no"](area.municipality);
);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
node["crossing"="traffic_signals"]["highway"~"^crossing$|^traffic_signals$"]["foot"!="no"][!"button_operated"](area.municipality);
node["crossing:signals"="yes"]["highway"~"^crossing$|^traffic_signals$"]["foot"!="no"][!"button_operated"](area.municipality);
);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15003000"]->.municipality;
(
node["crossing"="traffic_signals"]["highway"~"^crossing$|^traffic_signals$"]["foot"!="no"]["button_operated"](area.municipality);
node["crossing:signals"="yes"]["highway"~"^crossing$|^traffic_signals$"]["foot"!="no"]["button_operated"](area.municipality);
);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/traffic_signals_button/AddTrafficSignalsButton.kt).
```
nodes with
          (crossing = traffic_signals or crossing:signals = yes)
          and highway ~ crossing|traffic_signals
          and foot != no
          and !button_operated
```

</details>

# <img src="src/main/resources/sc_svg/leaf.svg" width="24"> <a name="qAddTreeLeafType"></a>AddTreeLeafType

<details>

This quest has a priority of **111.5** and will modify [](https://wiki.openstreetmap.org/wiki/) of elements with [Key:leaf_type](https://wiki.openstreetmap.org/wiki/Key:leaf_type), and it will count towards the following trophies: OUTDOORS.

## All
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(node["natural"="tree"](area.municipality);)->.all;
(node["natural"="tree"][~"^(species|genus|taxon)"~"."](area.municipality);)->.with_additionals;
(.all; - .with_additionals;);
(._;>;);
out meta;
```
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(node["natural"="tree"][!"leaf_type"](area.municipality);)->.all;
(node["natural"="tree"][!"leaf_type"][~"^(species|genus|taxon)"~"."](area.municipality);)->.with_additionals;
(.all; - .with_additionals;);
(._;>;);
out meta;
```
## Solved
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(node["natural"="tree"]["leaf_type"](area.municipality);)->.all;
(node["natural"="tree"]["leaf_type"][~"^(species|genus|taxon)"~"."](area.municipality);)->.with_additionals;
(.all; - .with_additionals;);
(._;>;);
out meta;
```

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/leaf_detail/AddTreeLeafType.kt).
```
nodes with
          natural = tree
          and !leaf_type
          and !~"(taxon|genus|species).*"
```

</details>

# <img src="src/main/resources/sc_svg/restaurant_vegetarian.svg" width="24"> <a name="qAddVegetarian"></a>AddVegetarian

<details>

This quest has a priority of **130.0** and will modify [diet:vegetarian](https://wiki.openstreetmap.org/wiki/diet:vegetarian) of elements with [Key:diet](https://wiki.openstreetmap.org/wiki/Key:diet), and it will count towards the following trophies: VEG, CITIZEN. This quest is resurveyed every 4 years.

Query for *All* is missing.
## Pending
```
[timeout:120];
area["de:amtlicher_gemeindeschluessel"="15002000"]->.municipality;
(
  nw["amenity"~"restaurant|cafe|fast_food|food_court"]["food"!="no"]
  ["diet:vegan"!="only"][!"diet:vegetarian"]
  (area.municipality);
nw["amenity"~"pub|nightclub|biergarten|bar"]["food"="yes"]
  ["diet:vegan"!="only"][!"diet:vegetarian"]
  (area.municipality);

nw["amenity"~"restaurant|cafe|fast_food|food_court"]["food"!="no"]
  ["diet:vegan"!="only"]["diet:vegetarian"!="only"]
  (area.municipality)->.allRestaurant;
nw["amenity"~"restaurant|cafe|fast_food|food_court"]["food"!="no"]
  ["diet:vegan"!="only"]["diet:vegetarian"!="only"]
  (area.municipality)(newer:"2021-01-28T16:56:10Z")->.newerRestaurant;

nw["amenity"~"pub|nightclub|biergarten|bar"]["food"="yes"]
  ["diet:vegan"!="only"]["diet:vegetarian"!="only"]
  (area.municipality)->.allPub;
nw["amenity"~"pub|nightclub|biergarten|bar"]["food"="yes"]
  ["diet:vegan"!="only"]["diet:vegetarian"!="only"]
  (area.municipality)->.newerPub;
  
(.allRestaurant; - .newerRestaurant;);
(.allPub; - .newerPub;);
  
);
(._;>;);
out meta;
```
Query for *Solved* is missing.

## Original element filter
See also the original file at [StreetComplete's github.com](https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/diet_type/AddVegetarian.kt).
```
nodes, ways with
        (
          amenity ~ restaurant|cafe|fast_food|food_court and food != no
          or amenity ~ pub|nightclub|biergarten|bar and food = yes
        )
        and diet:vegan != only and (
          !diet:vegetarian
          or diet:vegetarian != only and diet:vegetarian older today -4 years
        )
```

</details>

