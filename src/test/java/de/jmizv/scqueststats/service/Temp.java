package de.jmizv.scqueststats.service;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Temp {
    public static void main(String[] args) throws Exception {
        buildQuery();
        buildTemplateForJson();
    }

    static void buildTemplateForJson() throws IOException {
        var alternatives = readAlternatives();
        Gson gson = new Gson();
        System.out.println(gson.toJson(alternatives).replace("\\u003d", "="));
    }

    static List<List<String>> readAlternatives() throws IOException {
        List<List<String>> alternatives = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("./transform.txt"))) {
            alternatives.add(List.of(br.readLine()));
            br.readLine();
            var list = new ArrayList<String>();
            String line;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                if (line.isBlank()) {
                    alternatives.add(list);
                    list = new ArrayList<>();
                } else {
                    list.add(line);
                }
            }
            if (!list.isEmpty()) {
                alternatives.add(list);
            }
        }
        return alternatives;

    }

    static void buildQuery() throws IOException {
        List<String> results = generateCombinations(readAlternatives());
        System.out.println(results.size() + " permutations.");
        System.out.println("(");
        for (var sb : results) {
            System.out.println(sb);
        }
        System.out.println(");");
    }

    public static List<String> generateCombinations(List<List<String>> lists) {
        List<String> result = new ArrayList<>();
        generateCombinationsHelper(lists, 0, "", result);
        return result;
    }

    private static void generateCombinationsHelper(List<List<String>> lists, int depth, String current, List<String> result) {
        if (depth == lists.size()) {
            result.add(current);
            return;
        }

        for (String s : lists.get(depth)) {
            generateCombinationsHelper(lists, depth + 1, current + s, result);
        }
    }
}