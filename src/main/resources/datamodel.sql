DROP TABLE IF EXISTS highscore;
DROP TABLE IF EXISTS questelement;

DROP TABLE IF EXISTS quest;
DROP TABLE IF EXISTS municipality;
DROP TABLE IF EXISTS osmelement;
DROP TABLE IF EXISTS osmuser;

CREATE TABLE osmuser (
  id SERiAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE quest (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL UNIQUE,
  lastcheckdate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  enabled BOOLEAN NOT NULL DEFAULT false
);

CREATE TABLE municipality (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  extid VARCHAR(8) NOT NULL, -- amtlicher gemeindeschlüssel
  relationid BIGINT NOT NULL
);

CREATE TABLE osmelement (
  id SERIAL PRIMARY KEY,
  type CHAR(1) NOT NULL,
  extid BIGINT NOT NULL,
  CONSTRAINT element_uk UNIQUE (type,extid)
);

CREATE TABLE osm_point (
  id SERIAL PRIMARY KEY,
  p POINT NOT NULL,
  CONSTRAINT id_fk FOREIGN KEY (id) REFERENCES osmelement(id)
);

CREATE TABLE osm_way (
  id SERIAL PRIMARY KEY,
  l LINE NOT NULL,
  CONSTRAINT id_fk FOREIGN KEY (id) REFERENCES osmelement(id)
);

CREATE TABLE osm_relation (
  id SERIAL PRIMARY KEY,
  center POINT NOT NULL,
  CONSTRAINT id_fk FOREIGN KEY (id) REFERENCES osmelement(id)
);

CREATE TABLE questelement (
  id SERIAL PRIMARY KEY,
  element_fk_id BIGINT NOT NULL,
  quest_fk_id BIGINT NOT NULL,
  municipality_fk_id BIGINT NOT NULL,
  solved BOOLEAN NOT NULL DEFAULT false,
  active BOOLEAN NOT NULL DEFAULT true,
  CONSTRAINT element_fk FOREIGN KEY (element_fk_id) REFERENCES osmelement(id),
  CONSTRAINT quest_fk FOREIGN KEY (quest_fk_id) REFERENCES quest(id),
  CONSTRAINT municipality_fk FOREIGN KEY (municipality_fk_id) REFERENCES municipality(id),
  CONSTRAINT questelement_uk UNIQUE (element_fk_id,quest_fk_id,municipality_fk_id)
);

CREATE TABLE highscore (
  id SERIAL PRIMARY KEY
);


INSERT INTO quest (name,enabled)
VALUES
('AddAtmCashIn', true),
('AddAtmOperator', true),
('AddBenchBackrest', true),
('AddBenchStatusOnBusStop', true),
('AddBusStopName', true),
('AddInformationToTourism',true),
('AddMemorialType', true),
('AddParcelLockerBrand', false), -- Parcel Locker
('AddParcelLockerMailIn', false),-- quests   have
('AddParcelLockerPickup', false),-- been  removed
('AddPlaygroundAccess',true),
('AddSport', true),
('AddStepCount', true),
('AddStepsIncline', true),
('AddSummitRegister', false), -- not yet working as expected
('AddToiletAvailability', true),
('AddToiletsFee', true),
('AddTrafficCalmingType', true),
('AddTrafficSignalsButton', true),
('AddAcceptsCards',true),
('AddBikeParkingCover',true),
('AddBikeParkingType',true),
('AddCameraType',true),
('AddCarWashType',true),
('AddChargingStationCapacity',true),
('AddChargingStationOperator',true),
('AddFerryAccessMotorVehicle',true),
('AddFerryAccessPedestrian',true),
('AddVegetarian', true),
('AddPostboxCollectionTimes', true),
('AddPowerPolesMaterial', true),
('AddHairdresserCustomers', true),
('AddDefibrillatorLocation', true),
('AddBusStopShelter', true),
('AddBoardType', true),
('AddBuildingType', true),
('AddIsBuildingUnderground',true),
('AddBuildingLevels', true),
('AddAmenityCover',true),
('AddBikeParkingAccess',true),
('AddOrchardProduce',true),
('AddBikeParkingFee',true),
('AddTreeLeafType',true),
('AddForestLeafType',true),
('AddRoadName',true),
('AddAirCompressor',true),
('AddBikeParkingCapacity', true)
;

INSERT INTO municipality (name,extid,relationid) VALUES
('Halle (Saale)','15002000',62638),
('Magdeburg','15003000',62481),
('Ilsenburg','15085190',1352970),
('Gommern','15086055',85214),
('Hötensleben','15083320',1286642),
('Wernigerode','15085370',1352965);
