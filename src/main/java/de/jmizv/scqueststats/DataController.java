package de.jmizv.scqueststats;

import de.jmizv.scqueststats.service.QuestElementService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DataController {

    private final QuestElementService _questElementService;

    public DataController(QuestElementService questElementService) {
        _questElementService = questElementService;
    }

    @GetMapping(value = "/data/quest/municipality/{municipalityExtId}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody ResponseEntity<List<QuestElementService.QuestWrapper>> allQuestsForMunicipality(
            @PathVariable("municipalityExtId") String municipalityExtId) {
        return ResponseEntity.ok(_questElementService.statistics(municipalityExtId));
    }

    @GetMapping(value = "/data/quest/{questName}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody ResponseEntity<List<QuestElementService.MunicipalityAndQuestWrapper>> allMunicipalitesForQuests() {
        return ResponseEntity.ok(_questElementService.statistics());
    }

}
