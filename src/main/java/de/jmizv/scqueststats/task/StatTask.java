package de.jmizv.scqueststats.task;

import com.github.difflib.text.DiffRow;
import com.github.difflib.text.DiffRowGenerator;
import de.jmizv.scqueststats.model.ScQuest;
import de.jmizv.scqueststats.service.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.stream.Stream;

@Component
public class StatTask {

    private final OverpassService _overpassService;
    private final QuestService _questService;
    private final MunicipalityService _municipalityService;
    private final ElementService _elementService;
    private final QuestElementService _questElementService;

    public StatTask(OverpassService overpassService,
                    QuestService questService,
                    MunicipalityService municipalityService,
                    ElementService elementService,
                    QuestElementService questElementService) {
        _overpassService = overpassService;
        _questService = questService;
        _municipalityService = municipalityService;
        _elementService = elementService;
        _questElementService = questElementService;
    }

    @Scheduled(initialDelay = 0 * 120_000L, fixedRate = 86400_000L
            //, cron = "* */1 * * * *"
    )
    public void exec() throws Exception {
        //query();

        collectDataFromStreetCompleteSources();
    }

    private void collectDataFromStreetCompleteSources() throws IOException, NoSuchAlgorithmException {
        var quests = _questService.getSupportedQuest();
        var differences = new StringBuilder();
        var urlString = "https://raw.githubusercontent.com/streetcomplete/StreetComplete/master/app/src/main/java/de/westnordost/streetcomplete/quests/%location%";
        for (var quest : quests) {
            if (quest.getSourceFile().isBlank()) {
                System.err.println("Source file is not set for quest: " + quest);
                continue;
            }
            URL url = new URL(urlString.replace("%location%", quest.getSourceFile()));
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.addRequestProperty("Referer", "https://github.com");
            var ktFile = new String(conn.getInputStream().readAllBytes());
            var oldOriginalElementFilter = quest.getOriginalElementFilter();
            extractData(quest, ktFile);
            var fileHash = hash(ktFile);
            var elFilterHash = hash(quest.getOriginalElementFilter());
            if (quest.getSourceFileHash() == null || quest.getSourceFileHash().isBlank()) {
                quest.setSourceFileHash(fileHash);
                quest.setFilterHash(elFilterHash);
                _questService.writeQuestJsonBack(quest);
            } else {
                if (!quest.getFilterHash().equals(elFilterHash)) {
                    System.err.println("Element filter hash has changed for quest " + quest.getName() + "\nNew filter hash: " + elFilterHash + "\n(" + url + ")");
                    diff(quest.getName(), oldOriginalElementFilter, quest.getOriginalElementFilter(), differences);
                }
                if (!quest.getSourceFileHash().equals(fileHash)) {
                    System.err.println("Source file hash has changed for file " + quest.getSourceFile() + "\nNew file hash: " + fileHash + "\n(" + url + ")");
                }
            }
        }
        if (differences.isEmpty()) {
            System.out.println("Checked JSONs.");
        } else {
            System.err.println("Checked JSONs but found some differences. Check QUEST_DIFFS.md for details.");
            try (var fw = new FileWriter("./QUEST_DIFFS.md")) {
                fw.write(differences.toString());
                fw.flush();
            }
        }
    }

    private static void diff(String name, String oldOriginalElementFilter, String originalElementFilter, StringBuilder differences) {
        DiffRowGenerator generator = DiffRowGenerator.create()
                .showInlineDiffs(true)
                .inlineDiffByWord(true)
                .oldTag(f -> "**")
                .newTag(f -> "**")
                .build();

        List<DiffRow> rows = generator.generateDiffRows(
                List.of(oldOriginalElementFilter.split("\r?\n")),
                List.of(originalElementFilter.split("\r?\n")));
        StringBuilder sb = new StringBuilder("# " + name + "\n");
        sb.append("| # | old | new |\n");
        sb.append("|---|-----|-----|\n");
        boolean wasDifferent = false;
        int line = 0;
        for (DiffRow row : rows) {
            sb.append("| ");
            if (row.getNewLine().contains("**") || row.getOldLine().contains("**")) {
                wasDifferent = true;
                sb.append("**").append(++line).append("**");
            } else {
                sb.append(++line);
            }
            sb.append(" | ");
            var oldLine = row.getOldLine().replace("**", "").replace("|", "\\|").trim();
            if (!oldLine.isEmpty()) {
                sb.append("`").append(oldLine).append("`");
            }
            sb.append(" | ");
            var newLine = row.getNewLine().replace("**", "").replace("|", "\\|").trim();
            if (!newLine.isEmpty()) {
                sb.append("`").append(newLine).append("`");
            }
            sb.append(" |\n");
        }
        if (wasDifferent) {
            differences.append(sb);
        }
    }

    private static void extractData(ScQuest quest, String ktFile) {
        var sbFilter = new StringBuilder();
        var sbCountries = new StringBuilder();
        boolean beginFilterFound = false;
        boolean beginEnabledInCountriesFound = false;
        for (String line : ktFile.split("\r?\n")) {
            if ((line.contains("elementFilter") || line.contains("nodesFilter")) && sbFilter.isEmpty()) {
                int indexOfMultilineStringStart = line.indexOf("\"\"\"");
                int indexOfStringStart = line.indexOf("\"");
                if (indexOfMultilineStringStart == -1 && indexOfStringStart == -1) {
                    System.err.println("Could not find begin of elementFilter in " + ktFile);
                    continue;
                }
                if (indexOfStringStart > 0 && indexOfMultilineStringStart < 0) {
                    int lastIndexOfQuotation = line.indexOf("\"", indexOfStringStart + 1);
                    sbFilter.append(line.substring(indexOfStringStart + 1, lastIndexOfQuotation).trim());
                    continue;
                }
                beginFilterFound = true;
                if (indexOfMultilineStringStart == line.length() - 3) {
                    continue;
                }
                sbFilter.append(line.substring(indexOfMultilineStringStart + 3)).append("\n");
                continue;
            }
            if (beginFilterFound && line.contains("\"\"\"")) {
                int indexOfStringStart = line.indexOf("\"\"\"");
                sbFilter.append(line, 0, indexOfStringStart);
                sbFilter.append("\n");
                beginFilterFound = false;
                continue;
            }
            if (beginFilterFound) {
                sbFilter.append(line);
                sbFilter.append("\n");
                continue;
            }
            if (line.contains("wikiLink")) {
                int idx = line.indexOf("\"");
                int idx2 = line.indexOf("\"", idx + 1);
                if (idx < 0 || idx2 < 0) {
                    System.err.println("Found corrupted line with \"wikiLink\": " + line);
                    continue;
                }
                quest.setWikiLink(line.substring(idx + 1, idx2));
                continue;
            }
            if (!line.startsWith("import ") && line.contains(" achievements")) {
                int idx = line.indexOf("(");
                int idx2 = line.indexOf(")", idx + 1);
                if (idx < 0 || idx2 < 0) {
                    System.err.println("Found corrupted line with \"achievments\": " + line);
                    continue;
                }
                quest.setAchievements(Stream.of(line.substring(idx + 1, idx2).split(",")).map(String::trim).toList());
                continue;
            }
            if (line.contains("enabledInCountries ")) {
                int idx = line.indexOf("=");
                if (idx < 0) {
                    System.out.println("Line contains enabledInCountries but found no \"=\".");
                    continue;
                }
                sbCountries.append(line.substring(idx + 1).trim());
                if (line.contains("(") && line.contains(")")) {
                    continue;
                }
                beginEnabledInCountriesFound = true;
                continue;
            }
            if (beginEnabledInCountriesFound) {
                sbCountries.append(line);
                sbCountries.append("\n");
                if (line.contains(")")) {
                    beginEnabledInCountriesFound = false;
                }
                continue;
            }
            if (line.contains(" icon =")) {
                int idx = line.indexOf("=");
                quest.setIcon(line.substring(idx + 1).trim());
                continue;
            }
            if (line.contains("fun getTitle")) {
                int idx = line.indexOf("= R.");
                quest.setTitle(line.substring(idx + 2).trim());
            }
        }
        quest.setOriginalElementFilter(sbFilter.toString().trim());
        quest.setEnabledInCountries(sbCountries.toString().trim());
    }

    private void query() throws IOException {
        var quests = _questService.allQuest();
        var municipalities = _municipalityService.allMunicipalities();

        for (var quest : quests) {
            if (!quest.getName().equals("AddTreeLeafType")) {
                continue;
            }
            if (!quest.isEnabled()) {
                continue;
            }
            /*if (quest.getLastcheck().isAfter(Instant.now().minus(24, ChronoUnit.HOURS))) {
                continue;
            }*/
            for (var municipality : municipalities) {
                for (boolean solved : new boolean[]{false, true}) {
                    String query = _questService.generateQueryForQuestAndMunicipality(quest, municipality.getExtid(), solved);
                    try {
                        var queryResult = _overpassService.query(query);
                        long processedTime = System.currentTimeMillis();
                        System.out.println("Persist " + queryResult.elementSize() + " elements for quest " + quest.getName() + " and municipality " + municipality.getName() + " (solved=" + solved + ")");
                        var nodes = _elementService.createNodes(queryResult.getNodes());
                        var ways = _elementService.createWays(queryResult.getWays());
                        var relations = _elementService.createRelations(queryResult.getRelations());
                        System.out.println("Done. Now create the quest elements.");
                        nodes.forEach(n -> _questElementService.create(n.getId(), quest.getId(), municipality.getId(), solved));
                        ways.forEach(w -> _questElementService.create(w.getId(), quest.getId(), municipality.getId(), solved));
                        relations.forEach(r -> _questElementService.create(r.getId(), quest.getId(), municipality.getId(), solved));
                        System.out.println("Finished.");
                        Thread.sleep(Math.max(5_000 - (System.currentTimeMillis() - processedTime), 0));
                    } catch (RuntimeException | InterruptedException ex) {
                        System.err.println("Caught exception: " + ex.getClass());
                        ex.printStackTrace(System.err);
                    }
                    quest.setLastcheck(Instant.now());
                    _questService.update(quest);
/*

select count(*),
	(select name from quest where id = quest_fk_id),
	(select name from municipality where id = municipality_fk_id),
	solved
FROM questelement
group by quest_fk_id,municipality_fk_id,solved
order by 3,2,4

 */


                }
            }
        }
    }

    private static String hash(String input) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
        messageDigest.update(input.getBytes());
        return new String(Base64.getEncoder().encode(messageDigest.digest()));
    }
}