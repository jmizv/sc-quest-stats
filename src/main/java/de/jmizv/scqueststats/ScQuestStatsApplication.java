package de.jmizv.scqueststats;

import de.jmizv.scqueststats.model.ScQuest;
import de.jmizv.scqueststats.service.MunicipalityService;
import de.jmizv.scqueststats.service.QuestService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.Random;

@SpringBootApplication
@EnableScheduling
public class ScQuestStatsApplication implements CommandLineRunner {

    private final QuestService _questService;
    private final MunicipalityService _municipalityService;

    public ScQuestStatsApplication(QuestService questService,
                                   MunicipalityService municipalityService) {
        _questService = questService;
        _municipalityService = municipalityService;
    }

    @Override
    public void run(String... args) throws Exception {
        rebuildQuestMd();
    }

    private static String mapBooleanToEmoji(String in) {
        return in == null || in.isEmpty() ? "❌" : "✅";
    }

    private void rebuildQuestMd() throws IOException {
        var municipalities = _municipalityService.allMunicipalities();
        var rndm = new Random(111_0_112);
        var sbContent = new StringBuilder();
        var sbIndex = new StringBuilder();
        var sbOverview = new StringBuilder("""
                | Quest | All | Pending | Solved | Original |
                |-------|-----|---------|--------|----------|
                """);
        var quests = _questService.getSupportedQuest();


        sbIndex.append("<details>\n<summary>The following contains an overview on all ").append(quests.size()).append(" supported quests.</summary>\n\n");
        // add anchors and links like an index
        quests.stream().sorted(Comparator.comparing(ScQuest::getName)).forEach(q -> {
            sbOverview.append("| ").append(q.getName());
            if (q.isObsolete()) {
                sbOverview.append(" ❗️");
            }
            sbOverview.append(" | ").append(mapBooleanToEmoji(q.getQueries().getAll())).append(" | ")
                    .append(mapBooleanToEmoji(q.getQueries().getPending())).append(" | ")
                    .append(mapBooleanToEmoji(q.getQueries().getSolved())).append(" | ")
                    .append(mapBooleanToEmoji(q.getOriginalElementFilter())).append(" |\n");

            sbIndex.append("* [").append(q.getName()).append("](#q").append(q.getName()).append(")\n");
            var icon = q.getIcon().replace("R.drawable.ic_quest_", "");
            sbContent.append("# <img src=\"src/main/resources/sc_svg/");
            sbContent.append(icon);
            sbContent.append(".svg\" width=\"24\"> <a name=\"q").append(q.getName()).append("\"></a>").append(q.getName()).append("\n");
            if (q.isObsolete()) {
                sbContent.append("ℹ️ This quest is deprecated and no longer available in the current version of StreetComplete.\n");
            }
            sbContent.append("\n<details>\n\n");
            sbContent.append("This quest has a priority of **");
            sbContent.append(q.getDefaultPriority());
            sbContent.append("** and will modify [")
                    .append(q.getTag())
                    .append("](https://wiki.openstreetmap.org/wiki/")
                    .append(q.getTag())
                    .append(") of elements with [")
                    .append(q.getWikiLink())
                    .append("](https://wiki.openstreetmap.org/wiki/")
                    .append(q.getWikiLink())
                    .append("), and it will count towards the following trophies: ")
                    .append(String.join(", ", q.getAchievements())).append(".");
            if (!q.getResurvey().isBlank()) {
                sbContent.append(" This quest is resurveyed every ").append(q.getResurvey()).append(".");
            }
            sbContent.append("\n\n");
            if (q.getQueries().getAll() == null || q.getQueries().getAll().isBlank()) {
                sbContent.append("Query for *All* is missing.\n");
            } else {
                sbContent.append("## All\n```\n")
                        .append(_questService.generateCompleteQueryForMunicipality(q.getQueries().getAll(),
                                municipalities.get(rndm.nextInt(municipalities.size())).getExtid()))
                        .append("```\n");
            }
            if (q.getQueries().getPending().isBlank()) {
                sbContent.append("Query for *Pending* is missing.\n");
                System.err.println("Query for Pending is missing on quest " + q.getName());
            } else {
                sbContent.append("## Pending\n```\n")
                        .append(_questService.generateCompleteQueryForMunicipality(q.getQueries().getPending(),
                                municipalities.get(rndm.nextInt(municipalities.size())).getExtid()))
                        .append("```\n");
            }
            if (q.getQueries().getSolved().isBlank()) {
                sbContent.append("Query for *Solved* is missing.\n");
            } else {
                sbContent.append("## Solved\n```\n")
                        .append(_questService.generateCompleteQueryForMunicipality(q.getQueries().getSolved(),
                                municipalities.get(rndm.nextInt(municipalities.size())).getExtid()))
                        .append("```\n");
            }
            sbContent.append("\n");
            if (q.getOriginalElementFilter().isBlank()) {
                sbContent.append("The original element filter from the [StreetComplete source code]" +
                                 "(https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/")
                        .append(q.getSourceFile()).append(") is currently missing.\n");
            } else {
                sbContent.append("## Original element filter\n");
                sbContent.append("See also the original file at [StreetComplete's github.com]" +
                                 "(https://github.com/streetcomplete/StreetComplete/blob/master/app/src/main/java/de/westnordost/streetcomplete/quests/")
                        .append(q.getSourceFile()).append(").\n");
                sbContent.append("```\n").append(q.getOriginalElementFilter()).append("\n```\n");
            }
            sbContent.append("\n</details>\n\n");
        });
        File file = new File(".", "QUESTS.md");
        try (var printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file, StandardCharsets.UTF_8)))) {
            printWriter.write(sbIndex.toString());
            printWriter.write("</details>\n");
            printWriter.write("\n");
            printWriter.write("# Overview\n\n");
            printWriter.write(sbOverview.toString());
            printWriter.write("\n");
            printWriter.write(sbContent.toString());
            printWriter.flush();
            System.out.println("Wrote new QUEST.md file.");
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(ScQuestStatsApplication.class, args);
    }
}

