package de.jmizv.scqueststats.entity;


import jakarta.persistence.*;

@Entity
@Table(name = "municipality")
public class MunicipalityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long _id;

    @Column(name = "name")
    private String _name;
    @Column(name = "extid")
    private String _extid;
    @Column(name = "relationid")
    private Long _relationId;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getExtid() {
        return _extid;
    }

    public void setExtid(String extid) {
        _extid = extid;
    }

    public Long getRelationId() {
        return _relationId;
    }

    public void setRelationId(Long relationId) {
        _relationId = relationId;
    }
}
