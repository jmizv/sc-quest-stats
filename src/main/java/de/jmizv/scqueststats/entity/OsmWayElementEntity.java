package de.jmizv.scqueststats.entity;

import jakarta.persistence.*;
import org.postgresql.geometric.PGpath;


@Entity
@Table(name = "osm_way")
public class OsmWayElementEntity {

    @Id
    @Column(name = "id")
    private Long _id;

    @Column(name = "l")
    private PGpath l;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public PGpath getL() {
        return l;
    }

    public void setL(PGpath l) {
        this.l = l;
    }
}
