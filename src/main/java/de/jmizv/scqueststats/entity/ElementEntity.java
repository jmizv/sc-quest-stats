package de.jmizv.scqueststats.entity;


import jakarta.persistence.*;

@Entity
@Table(name = "osmelement")
public class ElementEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long _id;

    @Column(name = "type")
    private String type;
    @Column(name = "extid")
    private Long extid;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String typeToSet) {
        type = typeToSet;
    }

    public Long getExtid() {
        return extid;
    }

    public void setExtid(Long extid) {
        this.extid = extid;
    }
}
