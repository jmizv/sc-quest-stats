package de.jmizv.scqueststats.entity;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.geometric.PGpath;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;

public class PgPathUserType implements UserType<PGpath> {

    @Override
    public int getSqlType() {
        return Types.OTHER;
    }

    @Override
    public Class<PGpath> returnedClass() {
        return PGpath.class;
    }

    @Override
    public boolean equals(PGpath x, PGpath y) {
        if (x == null && y == null) {
            return true;
        }
        if (x == null) {
            return false;
        }
        return x.equals(y);
    }

    @Override
    public int hashCode(PGpath x) {
        return x.hashCode();
    }

    @Override
    public PGpath nullSafeGet(ResultSet rs, int position, SharedSessionContractImplementor session, Object owner) throws SQLException {
        return rs.getObject(position, PGpath.class);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, PGpath value, int index, SharedSessionContractImplementor session) throws SQLException {
        if (value == null) {
            st.setNull(index, getSqlType());
        }else {
            st.setObject(index, value, getSqlType());
        }
    }

    @Override
    public PGpath deepCopy(PGpath value) {
        PGpath path = new PGpath();
        path.open = value.open;
        if (value.points != null) {
            path.points = Arrays.copyOf(value.points, value.points.length);
        }
        return path;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(PGpath value) {
        return value;
    }

    @Override
    public PGpath assemble(Serializable cached, Object owner) {
        if (cached instanceof PGpath || cached == null) {
            return (PGpath) cached;
        }
        throw new IllegalArgumentException("Cannot cast object of class " + cached.getClass() + " to PGPoint.");
    }
}
