package de.jmizv.scqueststats.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "questelement")
public class QuestElementEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long _id;

    @Column(name = "element_fk_id")
    private Long elementId;
    @Column(name = "quest_fk_id")
    private Long questId;
    @Column(name = "municipality_fk_id")
    private Long municipalityId;
    @Column(name = "solved")
    private boolean _solved;
    @Column(name = "active")
    private boolean _active;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public Long getElementId() {
        return elementId;
    }

    public void setElementId(Long elementId) {
        this.elementId = elementId;
    }

    public Long getQuestId() {
        return questId;
    }

    public void setQuestId(Long questId) {
        this.questId = questId;
    }

    public Long getMunicipalityId() {
        return municipalityId;
    }

    public void setMunicipalityId(Long municipalityId) {
        this.municipalityId = municipalityId;
    }

    public boolean isSolved() {
        return _solved;
    }

    public void setSolved(boolean solved) {
        _solved = solved;
    }

    public boolean isActive() {
        return _active;
    }

    public void setActive(boolean active) {
        _active = active;
    }
}
