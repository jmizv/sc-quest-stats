package de.jmizv.scqueststats.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.Type;
import org.postgresql.geometric.PGpoint;


@Entity
@Table(name = "osm_way")
public class OsmRelationElementEntity {

    @Id
    @Column(name = "id")
    private Long _id;

    @Column(name = "l")
    @Type(value = PgPointUserType.class)
    private PGpoint center;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public PGpoint getCenter() {
        return center;
    }

    public void setCenter(PGpoint center) {
        this.center = center;
    }
}
