package de.jmizv.scqueststats.entity;

import jakarta.persistence.*;

import java.time.Instant;

@Entity
@Table(name = "quest")
public class QuestEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long _id;

    @Column(name = "name")
    private String _name;

    @Column(name = "lastcheckdate")
    private Instant _lastcheck;

    //@Column(name = "lastduration")
    //private Duration _lastDuration;

    @Column(name = "enabled")
    private boolean enabled;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public Instant getLastcheck() {
        return _lastcheck;
    }

    public void setLastcheck(Instant lastcheck) {
        _lastcheck = lastcheck;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "QuestEntity{" +
               "_name='" + _name + '\'' +
               '}';
    }
}
