package de.jmizv.scqueststats.entity;

import jakarta.persistence.*;
import org.hibernate.annotations.Type;
import org.postgresql.geometric.PGpoint;

@Entity
@Table(name = "osm_point")
public class OsmPointElementEntity {

    @Id
    @Column(name = "id")
    // refers a column in osmelement/ElementEntity
    private Long _id;

    @Column(name = "p")
    @Type(value = PgPointUserType.class)
    private PGpoint p;

    public Long getId() {
        return _id;
    }

    public void setId(Long id) {
        _id = id;
    }

    public PGpoint getP() {
        return p;
    }

    public void setP(PGpoint p) {
        this.p = p;
    }
}
