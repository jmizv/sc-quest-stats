package de.jmizv.scqueststats.entity;

import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.geometric.PGpoint;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class PgPointUserType implements UserType<PGpoint> {

    @Override
    public int getSqlType() {
        return Types.OTHER;
    }

    @Override
    public Class<PGpoint> returnedClass() {
        return PGpoint.class;
    }

    @Override
    public boolean equals(PGpoint x, PGpoint y) {
        if (x == null && y == null) {
            return true;
        }
        if (x == null) {
            return false;
        }
        return x.equals(y);
    }

    @Override
    public int hashCode(PGpoint x) {
        return x.hashCode();
    }

    @Override
    public PGpoint nullSafeGet(ResultSet rs, int position, SharedSessionContractImplementor session, Object owner) throws SQLException {
        return rs.getObject(position, PGpoint.class);
    }

    @Override
    public void nullSafeSet(PreparedStatement st, PGpoint value, int index, SharedSessionContractImplementor session) throws SQLException {
        if (value == null) {
            st.setNull(index, getSqlType());
        }else {
            st.setObject(index, value, getSqlType());
        }
    }

    @Override
    public PGpoint deepCopy(PGpoint value) {
        PGpoint point = new PGpoint();
        point.x = value.x;
        point.y = value.y;
        return point;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(PGpoint value) {
        return value;
    }

    @Override
    public PGpoint assemble(Serializable cached, Object owner) {
        if (cached instanceof PGpoint || cached == null) {
            return (PGpoint) cached;
        }
        throw new IllegalArgumentException("Cannot cast object of class " + cached.getClass() + " to PGPoint.");
    }
}
