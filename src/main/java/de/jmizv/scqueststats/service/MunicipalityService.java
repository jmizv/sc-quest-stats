package de.jmizv.scqueststats.service;

import de.jmizv.scqueststats.entity.MunicipalityEntity;
import de.jmizv.scqueststats.repository.MunicipalityRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MunicipalityService {

    private final MunicipalityRepository _municipalityRepository;

    public MunicipalityService(MunicipalityRepository municipalityRepository) {
        _municipalityRepository = municipalityRepository;
    }

    public List<MunicipalityEntity> allMunicipalities() {
        return _municipalityRepository.findAll();
    }
}
