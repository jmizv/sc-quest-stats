package de.jmizv.scqueststats.service;

import de.westnordost.osmapi.OsmConnection;
import de.westnordost.osmapi.map.MapDataApi;
import de.westnordost.osmapi.map.data.Node;
import de.westnordost.osmapi.map.data.Way;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OsmMapApiService {

    private final MapDataApi _api;

    public OsmMapApiService(@Value("${app.user-agent}") String userAgent) {
        var connection = new OsmConnection("https://api.openstreetmap.org/api/0.6/", userAgent);
        _api = new MapDataApi(connection);
    }

    public Node queryNode(long id) {
        return _api.getNode(id);
    }

    public List<Node> queryNodesForWay(Way way) {
        return queryNodes(way.getNodeIds());
    }

    public List<Node> queryNodes(List<Long> nodeIds) {
        return _api.getNodes(nodeIds);
    }

    public Way queryWay(long id) {
        return _api.getWay(id);
    }

    public List<Way> queryWays(List<Long> wayIds) {
        return _api.getWays(wayIds);
    }
}
