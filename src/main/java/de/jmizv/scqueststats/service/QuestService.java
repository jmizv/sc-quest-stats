package de.jmizv.scqueststats.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.jmizv.scqueststats.entity.QuestEntity;
import de.jmizv.scqueststats.model.ScQuest;
import de.jmizv.scqueststats.repository.QuestRepository;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Component
public class QuestService {

    private static final String QUERY_TEMPLATE = """
            [timeout:120];
            area["de:amtlicher_gemeindeschluessel"="%gemeindeschluessel%"]->.municipality;
            %query%
            (._;>;);
            out meta;
            """; //

    private final QuestRepository _questRepository;

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public QuestService(QuestRepository questRepository) {
        _questRepository = questRepository;
    }

    public List<QuestEntity> allQuest() {
        return _questRepository.findAll();
    }

    public List<ScQuest> getSupportedQuest() throws IOException {
        var quests = _questRepository.findAll();
        List<ScQuest> questsToReturn = new ArrayList<>();
        for (QuestEntity entity : quests) {
            var file = ResourceUtils.getFile("classpath:quests/" + entity.getName() + ".json");
            try (FileReader in = new FileReader(file)) {
                var scQuest = gson.fromJson(in, ScQuest.class);
                questsToReturn.add(scQuest);
            }
        }
        return questsToReturn;
    }

    public void writeQuestJsonBack(ScQuest quest) throws IOException {
        var file = new File("./" + quest.getName() + ".json");
        try (PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))) {
            writer.write(gson.toJson(quest));
            writer.flush();
        }
    }

    public String generateQueryForQuestAndMunicipality(QuestEntity quest, String municipalityExtId, boolean solved) throws IOException {
        File file = ResourceUtils.getFile("classpath:quests/" + quest.getName() + ".json");

        try (FileReader in = new FileReader(file)) {
            var scQuest = gson.fromJson(in, ScQuest.class);
            var query = solved ? scQuest.getQueries().getSolved() : scQuest.getQueries().getPending();
            return generateCompleteQueryForMunicipality(query, municipalityExtId);
        }
    }

    public String generateCompleteQueryForMunicipality(String queryFragment, String municipalityExtId) {
        var query = queryFragment;
        int start = query.indexOf("{{");
        while (start > 0) {
            int end = query.indexOf("}}", start);
            var duration = Duration.parse(query.substring(start + 2, end));
            query = query.substring(0, start)
                    + Instant.now().truncatedTo(ChronoUnit.SECONDS).plus(duration).toString()
                    + query.substring(end + 2);
            start = query.indexOf("{{");
        }

        return QUERY_TEMPLATE
                .replace("%query%", query)
                .replace("%gemeindeschluessel%", municipalityExtId);
    }

    public void update(QuestEntity quest) {
        _questRepository.saveAndFlush(quest);
    }
}
