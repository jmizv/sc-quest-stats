package de.jmizv.scqueststats.service;


import de.jmizv.umap.model.Umap;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UmapCreatorService {

    public Umap create() {
        return Umap.of("", List.of());
    }

}
