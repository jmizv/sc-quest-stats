package de.jmizv.scqueststats.service;

import de.jmizv.scqueststats.model.overpass.QueryResult;
import de.westnordost.osmapi.OsmConnection;
import de.westnordost.osmapi.map.data.BoundingBox;
import de.westnordost.osmapi.map.data.Node;
import de.westnordost.osmapi.map.data.Relation;
import de.westnordost.osmapi.map.data.Way;
import de.westnordost.osmapi.map.handler.MapDataHandler;
import de.westnordost.osmapi.overpass.OverpassMapDataApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OverpassService {

    private final String _userAgent;

    public OverpassService(@Value("${app.user-agent}") String userAgent) {
        _userAgent = userAgent;
    }

    private OsmConnection osm() {
        return new OsmConnection(
                //"https://api.openstreetmap.org/api/0.6/",
                "https://overpass-api.de/api/",
                _userAgent, null, 240_000);
    }

    public QueryResult query(String request) {
        var connection = osm();

        OverpassMapDataApi overpass = new OverpassMapDataApi(connection);

        List<Node> nodes = new ArrayList<>();
        List<Way> ways = new ArrayList<>();
        List<Relation> relations = new ArrayList<>();
        long start = System.currentTimeMillis();
        System.out.println("Started querying Overpass Map Data API:\n\n" + request);
        try {
            overpass.queryElements(request, new MapDataHandler() {
                @Override
                public void handle(BoundingBox boundingBox) {
                }

                @Override
                public void handle(Node node) {
                    nodes.add(node);
                }

                @Override
                public void handle(Way way) {
                    ways.add(way);
                }

                @Override
                public void handle(Relation relation) {
                    relations.add(relation);
                }
            });
            System.out.println("👍 Finished after " + (System.currentTimeMillis() - start) + "ms.");
        } catch (RuntimeException ex) {
            System.out.println("❌ Failed after " + (System.currentTimeMillis() - start) + "ms with: " + ex.getClass().getSimpleName());
            throw ex;
        }
        return QueryResult.of(nodes, ways, relations);
    }
}
