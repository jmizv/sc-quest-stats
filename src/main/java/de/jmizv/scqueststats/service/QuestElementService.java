package de.jmizv.scqueststats.service;

import de.jmizv.scqueststats.entity.QuestElementEntity;
import de.jmizv.scqueststats.repository.QuestElementRepository;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class QuestElementService {

    private final QuestElementRepository _elementRepository;

    public QuestElementService(QuestElementRepository elementRepository) {
        _elementRepository = elementRepository;
    }

    public Optional<QuestElementEntity> getByQuestMunicipality(long elementId, long questId, long municipalityId) {
        return _elementRepository.findByElementIdAndQuestIdAndMunicipalityId(elementId, questId, municipalityId);
    }

    public void create(long osmElementId, long questId, long municipalityId, boolean solved) {
        var questElementOptional = _elementRepository.findByElementIdAndQuestIdAndMunicipalityId(osmElementId, questId, municipalityId);
        if (questElementOptional.isPresent()) {
            var element = questElementOptional.get();
            if (element.isSolved() != solved) {
                element.setSolved(solved);
                element.setActive(true);
                _elementRepository.save(element);
            }
        } else {
            var questElement = new QuestElementEntity();
            questElement.setActive(true);
            questElement.setSolved(solved);
            questElement.setElementId(osmElementId);
            questElement.setQuestId(questId);
            questElement.setMunicipalityId(municipalityId);
            _elementRepository.save(questElement);
        }
    }

    public List<MunicipalityAndQuestWrapper> statistics() {
        var stats = _elementRepository.statistics();

        Map<String, Map<String, MunicipalityAndQuestWrapper>> intermediate = new TreeMap<>();
        for (QuestElementRepository.Wrapper wrapper : stats) {
            if (!intermediate.containsKey(wrapper.getMunicipality())) {
                intermediate.put(wrapper.getMunicipality(), new TreeMap<>());
            }
            if (!intermediate.get(wrapper.getMunicipality()).containsKey(wrapper.getQuest())) {
                intermediate.get(wrapper.getMunicipality()).put(wrapper.getQuest(), new MunicipalityAndQuestWrapper());
            }
            var q = intermediate.get(wrapper.getMunicipality()).get(wrapper.getQuest());
            q.municipality = wrapper.getMunicipality();
            q.quest = wrapper.getQuest();
            if (wrapper.isSolved()) {
                q.solved = wrapper.getAmount();
            } else {
                q.pending = wrapper.getAmount();
            }
        }

        return intermediate.values().stream().flatMap(p -> p.values().stream()).filter(k -> k.solved + k.pending > 0).toList();
    }

    public List<QuestWrapper> statistics(String municipalityExtId) {
        var stats  = _elementRepository.statistics(municipalityExtId);
        Map<String, QuestWrapper> intermediate = new TreeMap<>();
        for (QuestElementRepository.Wrapper wrapper : stats) {

            if (!intermediate.containsKey(wrapper.getQuest())) {
                intermediate.put(wrapper.getQuest(), new QuestWrapper());
            }
            var q = intermediate.get(wrapper.getQuest());
            q.quest = wrapper.getQuest();
            if (wrapper.isSolved()) {
                q.solved = wrapper.getAmount();
            } else {
                q.pending = wrapper.getAmount();
            }
        }
        return intermediate.values().stream().filter(k -> k.solved + k.pending > 0).toList();
    }


    public static class MunicipalityAndQuestWrapper {
        public String municipality;
        public String quest;
        public int solved;
        public int pending;
    }

    public static class QuestWrapper {
        public String quest;
        public int solved;
        public int pending;
    }
}
