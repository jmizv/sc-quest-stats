package de.jmizv.scqueststats.service;

import de.jmizv.scqueststats.entity.ElementEntity;
import de.jmizv.scqueststats.entity.OsmPointElementEntity;
import de.jmizv.scqueststats.entity.OsmRelationElementEntity;
import de.jmizv.scqueststats.entity.OsmWayElementEntity;
import de.jmizv.scqueststats.repository.ElementRepository;
import de.jmizv.scqueststats.repository.OsmPointElementRepository;
import de.jmizv.scqueststats.repository.OsmRelationElementRepository;
import de.jmizv.scqueststats.repository.OsmWayElementRepository;
import de.westnordost.osmapi.map.data.*;
import org.postgresql.geometric.PGpath;
import org.postgresql.geometric.PGpoint;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Component
public class ElementService {

    private final ElementRepository _elementRepository;
    private final OsmPointElementRepository _pointElementRepository;
    private final OsmRelationElementRepository _relationElementRepository;
    private final OsmWayElementRepository _wayElementRepository;
    private final OsmMapApiService _osmMapApiService;

    public ElementService(ElementRepository elementRepository,
                          OsmPointElementRepository pointElementRepository,
                          OsmRelationElementRepository relationElementRepository,
                          OsmWayElementRepository wayElementRepository,
                          OsmMapApiService osmMapApiService
    ) {
        _elementRepository = elementRepository;
        _pointElementRepository = pointElementRepository;
        _relationElementRepository = relationElementRepository;
        _wayElementRepository = wayElementRepository;
        _osmMapApiService = osmMapApiService;
    }

    public Optional<ElementEntity> getNode(long id) {
        return _elementRepository.findByTypeAndExtid("N", id);
    }

    public Optional<ElementEntity> getWay(long id) {
        return _elementRepository.findByTypeAndExtid("W", id);
    }

    public Optional<ElementEntity> getRelation(long id) {
        return _elementRepository.findByTypeAndExtid("R", id);
    }

    public void createOrUpdate(ElementEntity element) {
        _elementRepository.save(element);
    }

    public ElementEntity create(ElementEntity entity) {
        return _elementRepository.save(entity);
    }

    public OsmPointElementEntity create(OsmPointElementEntity entity) {
        return _pointElementRepository.save(entity);
    }

    public OsmWayElementEntity create(OsmWayElementEntity entity) {
        return _wayElementRepository.save(entity);
    }

    public OsmRelationElementEntity create(OsmRelationElementEntity entity) {
        return _relationElementRepository.save(entity);
    }

    List<ElementEntity> create(List<? extends Element> elements) {
        var resultList = new ArrayList<ElementEntity>();
        for (var el : elements) {
            Optional<ElementEntity> optionalElementEntity;
            String type;
            if (el instanceof Node) {
                optionalElementEntity = getNode(el.getId());
                type = "N";
            } else if (el instanceof Way) {
                optionalElementEntity = getWay(el.getId());
                type = "W";
            } else if (el instanceof Relation) {
                optionalElementEntity = getRelation(el.getId());
                type = "R";
            } else {
                throw new IllegalStateException("Unexpected OSM element: " + (el != null ? el.getId() : "<null>"));
            }
            if (optionalElementEntity.isEmpty()) {
                ElementEntity entity = new ElementEntity();
                entity.setExtid(el.getId());
                entity.setType(type);
                var elementEntity = create(entity);
                resultList.add(elementEntity);
                if (el instanceof Node node) {
                    var pointElement = new OsmPointElementEntity();
                    pointElement.setId(elementEntity.getId());
                    pointElement.setP(new PGpoint(node.getPosition().getLatitude(), node.getPosition().getLongitude()));
                    create(pointElement);
                } else if (el instanceof Way way) {
                    var wayElement = new OsmWayElementEntity();
                    wayElement.setId(elementEntity.getId());
                    wayElement.setL(toPolygon(_osmMapApiService.queryNodesForWay(way)));
                    create(wayElement);
                } else if (el instanceof Relation relation) {
                    var relationElement = new OsmRelationElementEntity();
                    relationElement.setId(elementEntity.getId());
                    relationElement.setCenter(calculateCenter(relation.getMembers()));
                    create(relationElement);
                }
            } else {
                resultList.add(optionalElementEntity.get());
            }
        }
        return resultList;
    }

    private PGpoint calculateCenter(List<RelationMember> relationMembers) {
        if (relationMembers.isEmpty()) {
            throw new IllegalStateException("Relation is empty.");
        }
        var nodeIds = relationMembers.stream()
                .filter(p -> p.getType() == Element.Type.NODE)
                .map(RelationMember::getRef)
                .toList();
        var nodes = _osmMapApiService.queryNodes(nodeIds);
        var wayIds = relationMembers.stream()
                .filter(p -> p.getType() == Element.Type.WAY)
                .map(RelationMember::getRef)
                .toList();
        var ways = _osmMapApiService.queryWays(wayIds);
        var wayMembers = ways.stream()
                .map(Way::getNodeIds)
                .map(_osmMapApiService::queryNodes)
                .flatMap(Collection::stream);
        var concat = Stream.concat(wayMembers, nodes.stream()).toList();
        double lat = 0;
        double lng = 0;
        for (Node n : concat) {
            lat += n.getPosition().getLatitude();
            lng += n.getPosition().getLongitude();
        }
        return new PGpoint(lat / concat.size(), lng / concat.size());
    }

    private static PGpath toPolygon(List<Node> nodes) {
        boolean open = nodes.get(0).equals(nodes.get(nodes.size() - 1));

        var pointArray = nodes.stream()
                .map(node -> new PGpoint(node.getPosition().getLatitude(), node.getPosition().getLongitude()))
                .toArray(PGpoint[]::new);
        return new PGpath(pointArray, open);
    }

    public List<ElementEntity> createNodes(List<Node> nodes) {
        return create(nodes);
    }

    public List<ElementEntity> createWays(List<Way> ways) {
        return create(ways);
    }

    public List<ElementEntity> createRelations(List<Relation> relations) {
        return create(relations);
    }
}
