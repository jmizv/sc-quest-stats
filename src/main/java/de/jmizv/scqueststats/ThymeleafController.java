package de.jmizv.scqueststats;

import de.jmizv.scqueststats.entity.MunicipalityEntity;
import de.jmizv.scqueststats.service.MunicipalityService;
import de.jmizv.scqueststats.service.QuestElementService;
import de.jmizv.scqueststats.service.QuestService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Comparator;

@Controller
public class ThymeleafController {

    private final MunicipalityService _municipalityService;
    private final QuestService _questService;
    private final QuestElementService _questElementService;

    public ThymeleafController(MunicipalityService municipalityService,
                               QuestService questService,
                               QuestElementService questElementService) {
        _municipalityService = municipalityService;
        _questService = questService;
        _questElementService = questElementService;
    }

    @GetMapping("/")
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("statistics", _questElementService.statistics());

        var municipalities = _municipalityService.allMunicipalities();
        municipalities.sort(Comparator.comparing(MunicipalityEntity::getName));
        modelMap.addAttribute("municipalities", municipalities);

        return "index";
    }
}
