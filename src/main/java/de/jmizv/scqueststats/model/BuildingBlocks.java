package de.jmizv.scqueststats.model;

import java.util.List;

public class BuildingBlocks {
    private List<List<String>> all;
    private List<List<String>> pending;
    private List<List<String>> solved;

    public List<List<String>> getAll() {
        return all;
    }

    public void setAll(List<List<String>> all) {
        this.all = all;
    }

    public List<List<String>> getPending() {
        return pending;
    }

    public void setPending(List<List<String>> pending) {
        this.pending = pending;
    }

    public List<List<String>> getSolved() {
        return solved;
    }

    public void setSolved(List<List<String>> solved) {
        this.solved = solved;
    }
}
