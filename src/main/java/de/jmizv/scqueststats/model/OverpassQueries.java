package de.jmizv.scqueststats.model;

public class OverpassQueries {
    private String all;
    private String pending;
    private String solved;
    private BuildingBlocks buildingBlocks;

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getSolved() {
        return solved;
    }

    public void setSolved(String solved) {
        this.solved = solved;
    }

    public BuildingBlocks getBuildingBlocks() {
        return buildingBlocks;
    }

    public void setBuildingBlocks(BuildingBlocks buildingBlocks) {
        this.buildingBlocks = buildingBlocks;
    }
}
