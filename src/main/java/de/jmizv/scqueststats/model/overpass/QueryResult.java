package de.jmizv.scqueststats.model.overpass;

import de.westnordost.osmapi.map.data.Node;
import de.westnordost.osmapi.map.data.Relation;
import de.westnordost.osmapi.map.data.Way;

import java.util.ArrayList;
import java.util.List;

public class QueryResult {

    private final List<Node> _nodes = new ArrayList<>();
    private final List<Way> _ways = new ArrayList<>();
    private final List<Relation> _relations = new ArrayList<>();

    public List<Node> getNodes() {
        return _nodes;
    }

    public List<Way> getWays() {
        return _ways;
    }

    public List<Relation> getRelations() {
        return _relations;
    }

    public int elementSize() {
        return _nodes.size() + _ways.size() + _relations.size();
    }

    public static QueryResult of(List<Node> nodes, List<Way> ways, List<Relation> relations) {
        var result = new QueryResult();
        result._nodes.addAll(nodes);
        result._ways.addAll(ways);
        result._relations.addAll(relations);
        return result;
    }
}
