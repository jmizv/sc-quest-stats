package de.jmizv.scqueststats.model;

import java.util.List;

public class ScQuest {
    private String name;
    private OverpassQueries queries;
    private String tag;
    private String note;
    private String resurvey;
    private String sourceFile;
    private String sourceFileHash;
    private String filterHash;
    private String originalElementFilter;
    private String wikiLink;
    private List<String> achievements;
    private String enabledInCountries;
    private String title;
    private String icon;
    private String defaultDisabledMessage;
    private double defaultPriority;
    private boolean obsolete = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OverpassQueries getQueries() {
        return queries;
    }

    public void setQueries(OverpassQueries queries) {
        this.queries = queries;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    public String getFilterHash() {
        return filterHash;
    }

    public void setFilterHash(String filterHash) {
        this.filterHash = filterHash;
    }

    public String getSourceFileHash() {
        return sourceFileHash;
    }

    public void setSourceFileHash(String sourceFileHash) {
        this.sourceFileHash = sourceFileHash;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getResurvey() {
        return resurvey;
    }

    public void setResurvey(String resurvey) {
        this.resurvey = resurvey;
    }

    public String getOriginalElementFilter() {
        return originalElementFilter;
    }

    public void setOriginalElementFilter(String originalElementFilter) {
        this.originalElementFilter = originalElementFilter;
    }

    public String getWikiLink() {
        return wikiLink;
    }

    public void setWikiLink(String wikiLink) {
        this.wikiLink = wikiLink;
    }

    public List<String> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<String> achievements) {
        this.achievements = achievements;
    }

    public String getEnabledInCountries() {
        return enabledInCountries;
    }

    public void setEnabledInCountries(String enabledInCountries) {
        this.enabledInCountries = enabledInCountries;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDefaultDisabledMessage() {
        return defaultDisabledMessage;
    }

    public void setDefaultDisabledMessage(String defaultDisabledMessage) {
        this.defaultDisabledMessage = defaultDisabledMessage;
    }

    public double getDefaultPriority() {
        return defaultPriority;
    }

    public void setDefaultPriority(double defaultPriority) {
        this.defaultPriority = defaultPriority;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isObsolete() {
        return obsolete;
    }

    public void setObsolete(boolean obsolete) {
        this.obsolete = obsolete;
    }

    @Override
    public String toString() {
        return "ScQuest{" +
               "name='" + name + '\'' +
               '}';
    }
}
