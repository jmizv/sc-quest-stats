package de.jmizv.scqueststats.repository;

import de.jmizv.scqueststats.entity.OsmWayElementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OsmWayElementRepository extends JpaRepository<OsmWayElementEntity, Long> {
}
