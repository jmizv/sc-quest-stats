package de.jmizv.scqueststats.repository;

import de.jmizv.scqueststats.entity.QuestEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestRepository extends JpaRepository<QuestEntity, Long> {
}
