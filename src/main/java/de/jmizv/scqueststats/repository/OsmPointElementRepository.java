package de.jmizv.scqueststats.repository;

import de.jmizv.scqueststats.entity.OsmPointElementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OsmPointElementRepository extends JpaRepository<OsmPointElementEntity, Long> {
}
