package de.jmizv.scqueststats.repository;

import de.jmizv.scqueststats.entity.ElementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ElementRepository extends JpaRepository<ElementEntity, Long> {

    Optional<ElementEntity> findByTypeAndExtid(String type, long extid);
}
