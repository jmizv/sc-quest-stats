package de.jmizv.scqueststats.repository;

import de.jmizv.scqueststats.entity.MunicipalityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MunicipalityRepository extends JpaRepository<MunicipalityEntity, Long> {
}
