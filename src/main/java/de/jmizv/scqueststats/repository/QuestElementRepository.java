package de.jmizv.scqueststats.repository;

import de.jmizv.scqueststats.entity.QuestElementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface QuestElementRepository extends JpaRepository<QuestElementEntity, Long> {
    Optional<QuestElementEntity> findByElementIdAndQuestIdAndMunicipalityId(long elementId, long questId, long municipalityId);

    @Query(nativeQuery = true, value = """
            SELECT
             (SELECT name FROM municipality WHERE id=municipality_fk_id) AS municipality,
             (SELECT name FROM quest WHERE id = quest_fk_id) AS quest,
             solved AS solved,
             count(*) AS amount
            FROM questelement
            GROUP BY quest_fk_id,municipality_fk_id,solved
            ORDER BY 1,2,3
            """)
    List<Wrapper> statistics();

    @Query(nativeQuery = true, value = """
                        SELECT
             (SELECT name FROM quest WHERE id = quest_fk_id) AS quest,
             solved AS solved,
             count(*) AS amount
            FROM questelement qe, municipality m
            WHERE qe.municipality_fk_id = m.id AND m.extid = ?
            GROUP BY quest_fk_id,solved
            ORDER BY 1,2
            """)
    List<Wrapper> statistics(String municipalityExtId);

    interface Wrapper {
        String getMunicipality();

        String getQuest();

        boolean isSolved();

        int getAmount();
    }
}
