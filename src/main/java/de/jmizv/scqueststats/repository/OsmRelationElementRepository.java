package de.jmizv.scqueststats.repository;

import de.jmizv.scqueststats.entity.OsmRelationElementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OsmRelationElementRepository extends JpaRepository<OsmRelationElementEntity, Long> {
}
